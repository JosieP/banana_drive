<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MenuController extends Controller
{
    public function get_categories()
    {
        $categories = \App\Category::all();
        $data = [];
        foreach ($categories as $category) {
            array_push($data, [
                'id' => $category->id,
                'name' => $category->name,
                'slug' => str_replace(' ', '-', strtolower($category->name)),
                'active' => false,
            ]);
        }
        return response()->json(['categories' => $data]);
    }
    public function get_foods(Request $request)
    {
        $foods = \App\FoodDrink::where('category_id', $request->input('id'))->orWhere('sub_category_id', $request->input('id'))->get();
        return response()->json(['foods' => $foods]);
    }
}
