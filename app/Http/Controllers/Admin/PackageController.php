<?php

namespace App\Http\Controllers\Admin;

use App\Package;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PackageController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin'])->except('get_category_foods');
        $this->middleware('is_admin')->except('index', 'get_category_foods');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('private.package');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $package = new Package();
            $package->fill($request->except('categories', 'addons'));
            
            $package->save();
            if ($request->input('type') == 'menu') {
                foreach ($request->input('categories') as $category) {
                    $package_list = new \App\PackageList();
                    $package_list->package_id = $package->id;
                    $package_list->category_id = $category['category_id'];
                    $package_list->food_id = $category['value']['id'];
                    $package_list->save();
                } 
            } 
            return response()->json(['msg' => '<b>' . $package->name . '</b> successfuly added!', 'success' => true]);
        } catch (\Throwable $th) {
            \Log::info($th);
            return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function show(Package $package)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function edit(Package $package)
    {
        if ($package->type == 'menu') {
            $package->lists = $this->get_lists($package->id);
                $package->description = '<ul>' . $this->lists($package) . '</ul><p style="color:red;">Note:The entree(s) can be changed depend on your likes, but the number of entree(s) will remain.</p>';
        } 
        return response()->json(['package' => $package]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Package $package)
    {
        try {
            $package->fill($request->except('categories', 'addons'));

            $package->save();
            if ($request->input('type') == 'menu') {
                $lists = \App\PackageList::where('package_id', $package->id)->delete();
                foreach ($request->input('categories') as $category) {
                    $package_list = new \App\PackageList();
                    $package_list->package_id = $package->id;
                    $package_list->category_id = $category['category_id'];
                    $package_list->food_id = $category['value']['id'];
                    $package_list->save();
                } 
            } 

            return response()->json(['msg' => '<b>' . $package->name . '</b> successfuly updated!', 'success' => true]);

        } catch (\Throwable $th) {
            \Log::info($th);
            return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function destroy(Package $package)
    {
        $lists = \App\PackageList::where('package_id', $package->id)->delete();

        if ($package->delete()) {
            return response()->json(['msg' => '<b>' . $package->name . '</b> successfuly deleted!', 'success' => true]);
        } else {
            return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
        }
    }

    public function get_packages()
    {
        $packages = Package::all();
        $data = [];
        foreach ($packages as $package) {
            if ($package->type == 'menu') {
                array_push($data, [
                    'id' => $package->id,
                    'name' => $package->name,
                    'type' => $package->type,
                    'status' => $package->status,
                    'package_type' => $package->package_type,
                    'food_max' => $package->food_max,
                    'price' => $package->price,
                    'lists' => $this->get_lists($package->id),
                    'description' => '<ul>' . $this->lists($package) . '</ul><p style="color:red;">Note:The entree(s) can be changed depend on your likes, but the number of entree(s) will remain.</p>'
                ]);
            } else {
                array_push($data, $package);
            }
        }
        return response()->json(['packages' => $data]);
    }

    public function get_lists($id)
    {
        $lists = \App\PackageList::where('package_id', $id)->get();
        $data = [];
        foreach ($lists as $list) {
            array_push($data, [
                'id' => $list->id,
                'category_id' => $list->category_id,
                'value' => \App\FoodDrink::where('id', $list->food_id)->first(),
                'category_name' => \App\Category::where('id', $list->category_id)->first()->name,
            ]);
        }

        return $data;
    }

    public function lists($menu) {
        $lists = \App\PackageList::where('package_id', $menu->id)->get();
        $str = '';
        foreach ($lists as $list) {
            $str .= '<li>' . \App\Category::find($list->category_id)->name . '      -       ' . \App\FoodDrink::find($list->food_id)->name .'</li>';
        }

        return $str;
    }

    public function get_category_foods(Request $request)
    {
        $foods = \App\FoodDrink::where('category_id', $request->input('id'))->orWhere('sub_category_id', $request->input('id'))->get();
        return response()->json(['categoryFoods' => $foods]);
    }
}
