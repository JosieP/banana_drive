<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:admin');
    }
    public function showLoginForm()
    {
        return view('private.auth.login');
    }

    public function login(Request $request)
    {
        //Validate the form data
        $this->validate($request, [
            'email'     =>      "required|email",
            'password'  =>      "required"
        ]);

        //Attempt to log the user in

        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) 
        {
            return redirect()->intended(route('admin-home'));
        }

        return redirect()->back()->withInput($request->only('email', 'remember'));
    }
}
