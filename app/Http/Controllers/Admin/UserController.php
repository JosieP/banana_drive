<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin']);
        $this->middleware('is_admin')->except('index', 'reset');
    }

    public function index()
    {
        return view('private.user');
    }

    public function get_users()
    {
        $newUsers = User::orderBy('id', 'DESC')
                        ->where('created_at', '>', Carbon::now()->subDays(3))
                        ->get();

        return response()->json(['totalUsers' => User::all(), 'newUsers' => $newUsers]);
    }


    public function store(Request $request)
    {
        if ($request->input('type') == 'customer') {
            $user = new User();
        } else {
            $user = new \App\Admin();
        }
        $user->fill($request->all());
        $user->password = Hash::make('BananaDrive');
        $saved = $user->save();

        if ($saved) {
            return response()->json(['msg' => '<b>' . $user->name . '</b> successfuly added!', 'success' => true]);
        } else {
            return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
        }
    }

    public function update(Request $request)
    {
        $type = substr($request->input('id'), 0, 1);
        if ($type == 'C') {
            $user = \App\User::find($request->input('orig_id'));
        } else if ($type == 'A') {
            $user = \App\Admin::find($request->input('orig_id'));
        }
        $user->firstname = $request->input('firstname');
        $user->lastname = $request->input('lastname');
        $user->email = $request->input('email');
        $user->type = $request->input('type');

        $saved = $user->save();

        if ($saved) {
            return response()->json(['msg' => '<b>' . $user->name . '</b> successfuly updated!', 'success' => true]);
        } else {
            return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
        }
    }
    public function destroy(Request $request)
    {
        // return $request;
        $type = substr($request->input('id'), 0, 1);
        if ($type == 'C') {
            $user = \App\User::find($request->input('orig_id'));
        } else if ($type == 'A') {
            $user = \App\Admin::find($request->input('orig_id'));
        }
        $details = \App\UserDetails::where('user_id', $user->id)->first();

        if ($details->delete()) {
            $user->delete();
            return response()->json(['msg' => '<b>' . $user->name . '</b> successfuly deleted!', 'success' => true]);
        } else {
            return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
        }
    }

    public function get_admins_users(Request $request)
    {
        if ($request->input('date') != '' || $request->input('date') != null) {
            $admins = \App\Admin::select('id', 'firstname', 'lastname', 'email', 'type', 'created_at')->whereDate('created_at', $request->input('date'));
            $users = User::select('id', 'firstname', 'lastname', 'email', 'type', 'created_at')->whereDate('created_at', $request->input('date'))->union($admins)->orderBy('created_at', 'ASC')->get();
        } else {
            $admins = \App\Admin::select('id', 'firstname', 'lastname', 'email', 'type', 'created_at');
            $users = User::select('id', 'firstname', 'lastname', 'email', 'type', 'created_at')->union($admins)->orderBy('created_at', 'ASC')->get();
        }   
        $data =[];
        foreach ($users as $user) {
            if ($user->type == 'customer') {
                $prefix = 'C';
            } else if ($user->type == 'admin') {
                $prefix = 'A';
            } else {
                $prefix = 'S';
            }

            array_push($data, [
                'id' => $prefix . str_pad($user->id, 8, 0, STR_PAD_LEFT),
                'orig_id' => $user->id,
                'firstname' => $user->firstname,
                'lastname' => $user->lastname,
                'email' => $user->email,
                'type' => $user->type,
                'creted_at' => $user->created_at
            ]);
        }
        return response()->json(['users' => $data]);
    }

    public function update_admin(Request $request, $id)
    {
        $admin = \App\Admin::find($id);
        $admin->firstname = $request->input('firstname');
        $admin->lastname = $request->input('lastname');
        $admin->email = $request->input('email');
        $admin->contact_number = '+63' . $request->input('contact_number');
        $admin->address = $request->input('address');
        if ($request->input('password') != null) {
            $admin->password = bcrypt($request->input('password'));
        }

        if ($admin->save()) {
            return response()->json(['msg' => '<b>' . $admin->firstname . '</b> successfuly updated!', 'success' => true]);
        } else {
            return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
        }
    }
}
