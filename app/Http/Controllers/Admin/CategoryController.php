<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin'])->except('get_categories');
        $this->middleware('is_admin')->except('index', 'get_categories');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('private.category');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = new Category();
        $category->fill($request->all());

        if ($category->save()) {
            return response()->json(['msg' => '<b>' . $category->name . '</b> successfuly added!', 'success' => true]);
        } else {
            return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $category->fill($request->all());

        if ($category->save()) {
            return response()->json(['msg' => '<b>' . $category->name . '</b> successfuly updated!', 'success' => true]);
        } else {
            return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        // try {
            // $foods = \App\FoodDrink::where('category_id', $category->id)->get();
            // foreach ($foods as $food) {
            //     $food->category_id = null;
            //     $food->save();
            // }

            // $food1s = \App\FoodDrink::where('category_id', $category->id)->orWhere('sub_category_id', $category->id)->get();
            // foreach ($food1s as $food) {
            //     $food->sub_category_id = null;
            //     $food->save();
            // }

            if ($category->delete()) {
                DB::commit();
                return response()->json(['msg' => '<b>' . $category->name . '</b> successfuly deleted!', 'success' => true]);
            } else {
                DB::rollback();
                return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
            }
        // } catch (Exception $e) {
        //     DB::rollback();
        //     \Log::info($e);
        // }
    }
    public function get_categories()
    {
        return response()->json(['categories' => Category::orderBy('name', 'ASC')->get()]);
    }
}
