<?php

namespace App\Http\Controllers\Admin;

use Excel;
use App\Province;
use Illuminate\Http\Request;
use App\Imports\ImportBarangay;
use App\Http\Controllers\Controller;

class ProvinceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware(['auth:admin']);
        $this->middleware('is_admin')->except('index');
    }
    
    public function index()
    {
        return view('private.province');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $province = new Province();
        $province->fill($request->all());

        if ($province->save()) {
            return response()->json(['msg' => '<b>' . $province->name . '</b> successfuly added!', 'success' => true]);
        } else {
            return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Province  $province
     * @return \Illuminate\Http\Response
     */
    public function show(Province $province)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Province  $province
     * @return \Illuminate\Http\Response
     */
    public function edit(Province $province)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Province  $province
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Province $province)
    {
        $province->fill($request->all());

        if ($province->save()) {
            return response()->json(['msg' => '<b>' . $province->name . '</b> successfuly updated!', 'success' => true]);
        } else {
            return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Province  $province
     * @return \Illuminate\Http\Response
     */
    public function destroy(Province $province)
    {
        if ($province->delete()) {
            return response()->json(['msg' => '<b>' . $province->name . '</b> successfuly deleted!', 'success' => true]);
        } else {
            return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
        }
    }

    public function update_excel(Request $request)
    {
        
        if ($request->hasFile('file')) {
            $file = $request->file('file');

            $data = Excel::import(new ImportBarangay, $file);

            return response()->json(['msg' => 'Successfully upload to database.', 'success' => true]);
        }
    }

    public function get_provinces_municipalities()
    {
        $ps = Province::all();
        $data = [];
        foreach ($ps as $p) {
            array_push($data, [
                'id' => $p->id,
                'price' => $p->price,
                'name' => $p->name,
                'type' => $p->type,
                'province_id' => $p->province_id,
                'value' => Province::where('province_id', $p->province_id)->first(),
            ]);
        }
        return response()->json(['provinces' => Province::all()]);
    }
    public function get_provinces()
    {
        return response()->json(['provinces' => Province::where('type', 'province')->get()]);
    }
    public function get_municipalities(Request $request)
    {
        return response()->json(['municipalities' => Province::where('type', 'municipality')->where('province_id', $request->input('id'))->get()]);
    }
}
