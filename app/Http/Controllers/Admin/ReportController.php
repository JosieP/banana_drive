<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin']);
        $this->middleware('is_admin')->except('index');
    }

    public function get_reservations_report()
    {
        $reservations = \App\Reservation::whereYear('created_at', '=', '2020')
                                        ->where('status', '<>', 'cancelling')
                                        ->Where('status', '<>', 'cancelled')
                                        ->get()
                                        ->groupBy(function($d) {
                                            return Carbon::parse($d->created_at)->format('m');
                                        });

        $creservations = \App\Reservation::whereYear('created_at', '=', '2020')
                                        ->where('status', 'cancelling')
                                        ->orWhere('status', 'cancelled')
                                        ->get()
                                        ->groupBy(function($d) {
                                            return Carbon::parse($d->created_at)->format('m');
                                        });
        $usermcount = [];
        $months = [];
        $cusermcount = [];
        $cmonths = [];

        foreach ($reservations as $key => $value) {
                $usermcount[(int)$key] = count($value);
        }

        foreach ($creservations as $key => $value) {
            $cusermcount[(int)$key] = count($value);
    }

        for($i = 1; $i <= 12; $i++){
            if(!empty($usermcount[$i])){
                $months[$i] = $usermcount[$i];    
            }else{
                $months[$i] = 0;    
            }
            if(!empty($cusermcount[$i])){
                $cmonths[$i] = $cusermcount[$i];    
            }else{
                $cmonths[$i] = 0;    
            }
        }
        $data = [];
        $cdata = [];

        foreach ($months as $key => $value) {
            array_push($data, $value);
        }
        foreach ($cmonths as $key => $value) {
            array_push($cdata, $value);
        }
        return response()->json(['reservations' => $data, 'creservations' => $cdata]);
    }

    public function get_bar_report()
    {        
        $waiting = \App\Reservation::whereMonth('created_at', date('m'))->where('status', 'waiting')->count();
        $pending = \App\Reservation::whereMonth('created_at', date('m'))->where('status', 'pending')->count();
        $approved = \App\Reservation::whereMonth('created_at', date('m'))->where('status', 'approved')->count();
        $completed = \App\Reservation::whereMonth('created_at', date('m'))->where('status', 'completed')->count();
        $cancelled = \App\Reservation::whereMonth('created_at', date('m'))->where('status', 'cancelled')->orWhere('status', 'cancelling')->count();

        $data = [$waiting, $pending, $approved, $completed, $cancelled];

        return response()->json(['data' => $data, 'month' => date('F')]);
    }
}
