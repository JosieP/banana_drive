<?php

namespace App\Http\Controllers\Admin;

use DB;
use Auth;
use DateTimeZone;
use Carbon\Carbon;
use App\Reservation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReservationController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin'])->except('get_reservation');
        $this->middleware('is_admin')->except('get_reservation');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('private.reservation');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function show(Reservation $reservation)
    {
        return view('private.show-reservation');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function edit(Reservation $reservation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reservation $reservation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reservation $reservation)
    {
        //
    }
    public function get_reservation($id)
    {
        $reservation = \App\Reservation::where('id', $id)->first();
        $reservation->address = ucwords($reservation->address);
        $reservation->name = ucwords(\App\User::select(DB::raw('CONCAT(firstname , " " , lastname) as name'))->where('id', $reservation->user_id)->pluck('name')->first());
        $reservation->municipality = ucwords(\App\Province::where('id', $reservation->municipality_id)->first()->name);
        $reservation->barangay = ucwords(\App\Province::where('id', $reservation->barangay_id)->first()->name);
        $reservation->service = \App\Service::where('id', $reservation->service_id)->first()->name;
        $reservation->billings = \App\Payment::where('reservation_id', $reservation->id)->get();
        $reservation->packages = $this->get_packages($reservation->id);
        $reservation->account = \App\RefundAccount::where('user_id', $reservation->user_id)->first();
        $reservation->actual_date = date('M d, Y', strtotime($reservation->start_actual_date));

        $end = strtotime(Carbon::parse($reservation->start_actual_date)->setTimezone(new DateTimeZone('Asia/Manila')));
        $start = strtotime(Carbon::now('Asia/Manila'));
        $total = $end - $start;
        $reservation->time = round((($total/60)/60)/24);

        return response()->json(['reservation' => $reservation]);
    }

    public function get_packages($id)
    {
        $ps = \App\ReservationPackage::where('reservation_id', $id)->get();
        $data = [];
        foreach ($ps as $p) {
            array_push($data, [
                'id' => $p->id,
                'package' => \App\Package::where('id', $p->package_id)->first(),
                'quantity' => $p->quantity,
                'days' => $p->no_days,
                'choice' => $p->meal_choice,
                'price' => ($p->price * $p->quantity) * $p->no_days,
                'lists' => $this->get_lists($p->id),
            ]);
        }
        return $data;
    }

    public function get_lists($id)
    {
        $lists = \App\ReservationPackageList::where('reservation_package_id', $id)->get();
        $data = [];
        foreach ($lists as $list) {
            array_push($data, [
                'id' => $list->id,
                'category_id' => $list->category_id,
                'value' => \App\FoodDrink::where('id', $list->food_id)->first(),
                'category_name' => \App\Category::where('id', $list->category_id)->first()->name,
            ]);
        }

        return $data;
    }

    public function get_reservations(Request $request)
    {
        if ($request->input('date') != '' || $request->input('date') != null) {
            $reservations = \App\Reservation::whereDate('created_at', $request->input('date'))->orderBy('created_at', 'DESC')->get();
        } else {
            $reservations = \App\Reservation::orderBy('created_at', 'DESC')->get();
        }        
        $data = [];
        foreach ($reservations as $reservation) {
            $end = strtotime(Carbon::parse($reservation->start_actual_date)->setTimezone(new DateTimeZone('Asia/Manila')));
            $start = strtotime(Carbon::now('Asia/Manila'));
            $total = $end - $start;
            $time = round((($total/60)/60)/24);

            array_push($data, [
                'orig_id' => $reservation->id,
                'id' => str_pad($reservation->id, 8, 0, STR_PAD_LEFT),
                'date' => $reservation->start_actual_date,
                'amount' => $reservation->amount,
                'time' => $time,
                'amount_paid' => $reservation->amount_paid,
                'user_id' => $reservation->user_id,
                'username' => \App\User::select(DB::raw('CONCAT(firstname , " " , lastname) as name'))->where('id', $reservation->user_id)->pluck('name')->first(),
                'service_id' => $reservation->service_id,
                'service_name' => \App\Service::where('id', $reservation->service_id)->first()->name,
                'municipality_id' => $reservation->municipality_id,
                'municipality_name' => \App\Province::where('id', $reservation->municipality_id)->first()->name,
                'address' => $reservation->address,
                'status' => $reservation->status,
                'billing' => \App\Payment::where('reservation_id', $reservation->id)->first(),
                'created_at' => $reservation->created_at,
            ]);
        }
        return response()->json(['reservations' => $data, 'today_reservations' => \App\Reservation::whereDate('created_at', '=', date('Y-m-d'))->get()->count()]);
    }

    public function update_status(Request $request, $id)
    {
        $reservation = \App\Reservation::find($id);
        $reservation->status = $request->input('status');
        $reservation->save();

        if ($request->input('status') == 'cancelled') {
            $ps = \App\Payment::where('reservation_id', $reservation->id)->get();

            foreach ($ps as $p) {
                $p->status = 'cancelled';
                $p->save();
            }
        }


        \App\StatusLog::create([
            'user_id' => Auth::user()->id,
            'reservation_id' => $reservation->id,
            'tag' => 'update-reservation',
            'status' => $reservation->status,
            'notes' => $request->input('notes')
        ]);
        \App\History::create([
            'user_id' => $reservation->user_id,
            'reservation_id' => $reservation->id,
            'tag' => 'admin-update-reservation',
            'comment' => $request->input('notes')
        ]);

        if (app('App\Http\Controllers\ReservationController')->api() != null) {
            \Telnyx\Telnyx::setApiKey(app('App\Http\Controllers\ReservationController')->api());
                
                $number = \App\UserDetails::where('user_id', $reservation->user_id)->first();
    
                $new_message = \Telnyx\Message::Create([
                                    'from' => '+17733606711', 
                                    'to' => '+63' . $reservation->contact_number,
                                    'text' => 'Banana Drive Catering : Admin update the status of your reservation. Please check on website.'
                                ]);
        }

        return response()->json(['msg' => 'Reservation status move to ' . $reservation->status . '!', 'success' => true]);
    }
}
