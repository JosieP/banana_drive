<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\User;
use App\Admin;
use App\UserDetails;
use Matrix\Exception;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class UserDetailsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin']);
        $this->middleware('is_admin')->except('index');
    }
    
    public function index()
    {
        return view('private.user-details');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            
            $details = new UserDetails();
    
            if ($request->input('id') != null) {
                $name = User::select(DB::raw('CONCAT(firstname , " " , lastname) as name'))->where('id', $request->input('id'))->pluck('name')->first();
                $details->user_id = $request->input('id');
            } else {
                $user = new User();
                $user->fill($request->all());
                $user->password = Hash::make('BananaDrive');
                $saved = $user->save();
                $name = User::select(DB::raw('CONCAT(firstname , " " , lastname) as name'))->where('id', $user->id)->pluck('name')->first();
                $details->user_id = $user->id;
            }
            $details->gender = $request->input('gender');
            $details->birthdate = $request->input('birthdate');
            $details->status = $request->input('status');
            $details->address = $request->input('address');
            $details->contact = $request->input('contact');
    
            if ($details->save()) {
				DB::commit();
                return response()->json(['msg' => '<b>' . $name . '</b> successfuly added!', 'success' => true]);
            } else {
				DB::rollback();
                return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
            }
        } catch (Exception $e) {
            DB::rollback();
            \Log::info($e);
			return response()->json(['success' => false, 'error' => $e->errorMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserDetails $user_detail)
    {

        $name = User::select(DB::raw('CONCAT(firstname , " " , lastname) as name'))->where('id', $user_detail->id)->pluck('name')->first();

        $user_detail->fill($request->all());
        if ($user_detail->save()) {
            return response()->json(['msg' => '<b>' . $name . '</b> successfuly added!', 'success' => true]);
        } else {
            return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserDetails $user_detail)
    {
        $user = \App\User::find($user_detail->user_id);
        if ($user_detail->delete()) {
            $user->delete();
            return response()->json(['msg' => '<b>' . $user->name . '</b> successfuly deleted!', 'success' => true]);
        } else {
            return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
        }
    }

    public function get_user_details()
    {
        $user_ids = User::pluck('id')->toArray();
        $detail_ids = UserDetails::pluck('user_id')->toArray();
        $result = array_diff($user_ids, $detail_ids);
        $data1 = [];
        foreach ($result as $key => $value) {
            array_push($data1, [
                'label' => User::select(DB::raw('CONCAT(firstname , " " , lastname) as name'))->where('id', $value)->pluck('name')->first(),
                'value' => User::where('id', $value)->pluck('id')->first(),
            ]);
        };

        $details = UserDetails::all();
        $data = [];
        foreach ($details as $detail) {
            $user = User::where('id', $detail->user_id)->first();

            array_push($data, [
                'id' => $detail->id,
                'email' => $user->email,
                'type' => $user->type,
                'name' => $user->firstname . ' ' . $user->lastname,
                'birthdate' => $detail->birthdate,
                'gender' => $detail->gender,
                'status' => $detail->status,
                'address' => $detail->address,
                'contact' => $detail->contact,
            ]);
        }

        return response()->json(['userDetails' => $data, 'users' => $data1]);
    }
}
