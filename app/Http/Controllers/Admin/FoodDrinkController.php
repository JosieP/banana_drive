<?php

namespace App\Http\Controllers\Admin;

use Storage;
use App\FoodDrink;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FoodDrinkController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin']);
        $this->middleware('is_admin')->except('index');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('private.foods_drinks');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = \Request::file('file');
        
        $food = new FoodDrink();
        $food->fill($request->all());
        
        if ($file != null) {
            $contents = file_get_contents($file);
            $food->photo = 'foods-' . $food->id . mt_rand() . '.' . $file->getClientOriginalExtension();
            $food->filename .= $file->getClientOriginalName();
            $path = 'foods/' . $food->photo;
            Storage::disk('public')->put($path, $contents);
        }


        if ($food->save()) {
            return response()->json(['msg' => '<b>' . $food->name . '</b> successfuly added!', 'success' => true]);
        } else {
            return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FoodDrink  $foodDrink
     * @return \Illuminate\Http\Response
     */
    public function show(FoodDrink $foodDrink)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FoodDrink  $foodDrink
     * @return \Illuminate\Http\Response
     */
    public function edit(FoodDrink $foodDrink)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FoodDrink  $foodDrink
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FoodDrink $foodDrink)
    {
        $foodDrink->fill($request->all());

        if ($foodDrink->save()) {
            return response()->json(['msg' => '<b>' . $foodDrink->name . '</b> successfuly updated!', 'success' => true]);
        } else {
            return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FoodDrink  $foodDrink
     * @return \Illuminate\Http\Response
     */
    public function destroy(FoodDrink $foodDrink)
    {
        if ($foodDrink->delete()) {
            return response()->json(['msg' => '<b>' . $foodDrink->name . '</b> successfuly deleted!', 'success' => true]);
        } else {
            return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
        }
    }

    public function get_food_drinks()
    {
        $foods = FoodDrink::all();
        $data = [];
        foreach ($foods as $food) {
            array_push($data, [
                'id' => $food->id,
                'name' => $food->name,
                'link' => $food->photo,
                'filename' => $food->filename,
                'category' => \App\Category::where('id', $food->category_id)->pluck('name')->first(),
                'category_id' => $food->category_id,
                'sub_category' => \App\Category::where('id', $food->sub_category_id)->pluck('name')->first(),
                'sub_category_id' => $food->sub_category_id,
                'description' => $food->description,
            ]);
        }
        return response()->json(['foods' => $data]);
    }

    public function update_photo(Request $request, $id) 
    {
        $food = \App\FoodDrink::find($id);
        $file = \Request::file('file');
        
        $contents = file_get_contents($file);
        $food->photo = 'foods-' . $food->id . mt_rand() . '.' . $file->getClientOriginalExtension();
        $food->filename .= $file->getClientOriginalName();
        $path = 'foods/' . $food->photo;
        Storage::disk('public')->put($path, $contents);

        if ($food->save()) {
            return response()->json(['msg' => '<b>' . $food->name . '</b> photo successfuly updated!', 'success' => true]);
        } else {
            return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
        }
    }
}
