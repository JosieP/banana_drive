<?php

namespace App\Http\Controllers;

use App\ReservationPackageList;
use Illuminate\Http\Request;

class ReservationPackageListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ReservationPackageList  $reservationPackageList
     * @return \Illuminate\Http\Response
     */
    public function show(ReservationPackageList $reservationPackageList)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ReservationPackageList  $reservationPackageList
     * @return \Illuminate\Http\Response
     */
    public function edit(ReservationPackageList $reservationPackageList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ReservationPackageList  $reservationPackageList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReservationPackageList $reservationPackageList)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ReservationPackageList  $reservationPackageList
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReservationPackageList $reservationPackageList)
    {
        //
    }
}
