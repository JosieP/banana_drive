<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Storage;
use DateTimeZone;
use Carbon\Carbon;
use App\StatusLog;
use Illuminate\Http\Request;

class StatusLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $sls = StatusLog::where('reservation_id', $id)->orderBy('id', 'DESC')->get();
        $data = [];
        foreach ($sls as $sl) {
            $user = \App\User::select(DB::raw('CONCAT(firstname , " " , lastname) as name'))->where('id', $sl->user_id)->pluck('name')->first();
            $start = strtotime($sl->created_at->setTimezone(new DateTimeZone('Asia/Manila')));
            $end = strtotime(Carbon::now('Asia/Manila'));
            $total = $end - $start; 
            if ($total > 60) {
                // min
                $time = round($total/60) . ' minute(s) ago';
                // hr
                if ($total/60 > 60) {
                    $time = round(($total/60)/60) . ' hour(s) ago';
                }
                // d
                if ((($total/60)/60) > 24) {
                    $time = round((($total/60)/60)/24) . ' day(s) ago';
                }
                // w
                if (((($total/60)/60)/24) > 7) {
                    $time = round(((($total/60)/60)/24)/7) . ' week(s) ago';
                }
                // m
                if ((((($total/60)/60)/24)/7) > 4) {
                    $time = round((((($total/60)/60)/24)/7)/4) . ' month(s) ago';
                }
            } else {
                $time = ($total) . ' second(s) ago';
            }

            if ($sl->tag == 'create-reservation') {
                array_push($data, [
                    'id' => $sl->id,
                    'user' => $user,
                    'date' => date('Y-m-d', strtotime($sl->created_at)),
                    'reservation_id' => $sl->reservation_id,
                    'body' => 'Your reservation status is waiting',
                    'time' => $time,
                    'notes' => $sl->notes
                ]);
            }
            if ($sl->tag == 'send-payment') {
                array_push($data, [
                    'id' => $sl->id,
                    'user' => $user,
                    'date' => date('Y-m-d', strtotime($sl->created_at)),
                    'reservation_id' => $sl->reservation_id,
                    'body' => 'You send a payment.Your reservation status is pending.',
                    'time' => $time,
                    'notes' => $sl->notes
                ]);
            }
            if ($sl->tag == 'create-payment') {
                array_push($data, [
                    'id' => $sl->id,
                    'user' => $user,
                    'date' => date('Y-m-d', strtotime($sl->created_at)),
                    'reservation_id' => $sl->reservation_id,
                    'body' => 'Your reservation status is moved to pending.',
                    'time' => $time,
                    'notes' => $sl->notes
                ]);
            }
            if ($sl->tag == 'update-status-payment') {
                array_push($data, [
                    'id' => $sl->id,
                    'user' => 'Admin',
                    'date' => date('Y-m-d', strtotime($sl->created_at)),
                    'reservation_id' => $sl->reservation_id,
                    'body' => 'Your payment status is moved to complete.',
                    'time' => $time,
                    'notes' => $sl->notes
                ]);
            }
            
            if ($sl->tag == 'update-reservation') {
                array_push($data, [
                    'id' => $sl->id,
                    'user' => 'Admin',
                    'date' => date('Y-m-d', strtotime($sl->created_at)),
                    'reservation_id' => $sl->reservation_id,
                    'body' => 'Your reservation is updated. Check the status.',
                    'time' => $time,
                    'notes' => $sl->notes
                ]);
            }
        }
        return response()->json(['logs' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StatusLog  $statusLog
     * @return \Illuminate\Http\Response
     */
    public function show(StatusLog $statusLog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StatusLog  $statusLog
     * @return \Illuminate\Http\Response
     */
    public function edit(StatusLog $statusLog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StatusLog  $statusLog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StatusLog $statusLog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StatusLog  $statusLog
     * @return \Illuminate\Http\Response
     */
    public function destroy(StatusLog $statusLog)
    {
        //
    }
}
