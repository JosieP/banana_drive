<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use DateTimeZone;
use Carbon\Carbon;
use App\History;
use Illuminate\Http\Request;

class HistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index()
    {
        $hs = History::where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->get();
        $data = [];
        foreach ($hs as $h) {
            $user = \App\User::select(DB::raw('CONCAT(firstname , " " , lastname) as name'))->where('id', $h->user_id)->pluck('name')->first();
            $start = strtotime($h->created_at->setTimezone(new DateTimeZone('Asia/Manila')));
            $end = strtotime(Carbon::now('Asia/Manila'));
            $total = $end - $start; 
            if ($total > 60) {
                // min
                $time = round($total/60) . ' minute(s) ago';
                // hr
                if ($total/60 > 60) {
                    $time = round(($total/60)/60) . ' hour(s) ago';
                }
                // d
                if ((($total/60)/60) > 24) {
                    $time = round((($total/60)/60)/24) . ' day(s) ago';
                }
                // w
                if (((($total/60)/60)/24) > 7) {
                    $time = round(((($total/60)/60)/24)/7) . ' week(s) ago';
                }
                // m
                if ((((($total/60)/60)/24)/7) > 4) {
                    $time = round((((($total/60)/60)/24)/7)/4) . ' month(s) ago';
                }
            } else {
                $time = ($total) . ' second(s) ago';
            }

            if ($h->tag == 'create-reservation') {
                array_push($data, [
                    'id' => $h->id,
                    'user' => $user,
                    'date' => date('Y-m-d', strtotime($h->created_at)),
                    'reservation_id' => $h->reservation_id,
                    'body' => 'You have successfully created a Reservation. Please send a proof of your payment.',
                    'time' => $time,
                ]);
            }
            if ($h->tag == 'cancel-reservation') {
                array_push($data, [
                    'id' => $h->id,
                    'user' => $user,
                    'date' => date('Y-m-d', strtotime($h->created_at)),
                    'reservation_id' => $h->reservation_id,
                    'body' => 'You have successfully cancelled a Reservation.',
                    'time' => $time,
                ]);
            }
            if ($h->tag == 'update-user') {
                array_push($data, [
                    'id' => $h->id,
                    'user' => $user,
                    'date' => date('Y-m-d', strtotime($h->created_at)),
                    'reservation_id' => $h->reservation_id,
                    'body' => 'You have successfully update you account.',
                    'time' => $time,
                ]);
            }
            if ($h->tag == 'create-payment') {
                array_push($data, [
                    'id' => $h->id,
                    'user' => $user,
                    'date' => date('Y-m-d', strtotime($h->created_at)),
                    'reservation_id' => $h->reservation_id,
                    'body' => 'You have successfully send a payment on your reservation ' . \App\Reservation::find($h->reservation_id)->title . '.',
                    'time' => $time,
                ]);
            }
            if ($h->tag == 'admin-update-reservation') {
                array_push($data, [
                    'id' => $h->id,
                    'user' => $user,
                    'date' => date('Y-m-d', strtotime($h->created_at)),
                    'reservation_id' => $h->reservation_id,
                    'body' => 'Admin update your reservation ' . \App\Reservation::find($h->reservation_id)->title . '. Please check the status of your reservation.',
                    'time' => $time,
                ]);
            }
            if ($h->tag == 'admin-update-reservation-fee') {
                array_push($data, [
                    'id' => $h->id,
                    'user' => $user,
                    'date' => date('Y-m-d', strtotime($h->created_at)),
                    'reservation_id' => $h->reservation_id,
                    'body' => 'Admin approved your reservation fee ' . \App\Reservation::find($h->reservation_id)->title . '. Please send the remaining payment 1 week before the event due.',
                    'time' => $time,
                ]);
            }
            
            if ($h->tag == 'update-reservation') {
                array_push($data, [
                    'id' => $h->id,
                    'user' => $user,
                    'date' => date('Y-m-d', strtotime($h->created_at)),
                    'reservation_id' => $h->reservation_id,
                    'body' => \App\Reservation::find($h->reservation_id)->title . ' was updated. Please check the details.',
                    'time' => $time,
                ]);
            }
            if ($h->tag == 'send-payment') {
                array_push($data, [
                    'id' => $h->id,
                    'user' => $user,
                    'date' => date('Y-m-d', strtotime($h->created_at)),
                    'reservation_id' => $h->reservation_id,
                    'body' => 'You have send a payment to ' . \App\Reservation::find($h->reservation_id)->title . '.',
                    'time' => $time,
                ]);
            }
        }
        return response()->json(['histories' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\History  $history
     * @return \Illuminate\Http\Response
     */
    public function show(History $history)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\History  $history
     * @return \Illuminate\Http\Response
     */
    public function edit(History $history)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\History  $history
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, History $history)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\History  $history
     * @return \Illuminate\Http\Response
     */
    public function destroy(History $history)
    {
        //
    }
}
