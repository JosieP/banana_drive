<?php

namespace App\Http\Controllers;

use App\DisabledDates;
use Illuminate\Http\Request;

class DisabledDatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['dates' => DisabledDates::pluck('d_date')]);
    }

    public function get_disabled_dates()
    {
        return response()->json(['dates' => DisabledDates::get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $date = DisabledDates::firstOrNew([
            'd_date' => $request->input('d_date'),
        ]);
        // $date->fill($request->all());

        if ($date->save()) {
            return response()->json(['msg' => '<b>' . $date->d_date . '</b> successfuly disable!', 'success' => true]);
        } else {
            return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DisabledDates  $disabledDates
     * @return \Illuminate\Http\Response
     */
    public function show(DisabledDates $disabledDates)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DisabledDates  $disabledDates
     * @return \Illuminate\Http\Response
     */
    public function edit(DisabledDates $disabledDates)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DisabledDates  $disabledDates
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DisabledDates $disabledDates)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DisabledDates  $disabledDates
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $disabledDates = DisabledDates::find($id);
        
        if ($disabledDates->delete()) {
            return response()->json(['msg' => '<b>' . $disabledDates->d_date . '</b> successfuly enable!', 'success' => true]);
        } else {
            return response()->json(['msg' => 'Failed to delete data!', 'success' => false]);
        }
    }
}
