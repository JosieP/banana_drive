<?php

namespace App\Http\Controllers;

use Mail;
use DateTimeZone;
use App\ContactUs;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ContactUsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $messages = ContactUs::orderby('id', 'DESC')->get();
        $data = [];
        foreach ($messages as $message) {
            $start = strtotime($message->created_at->setTimezone(new DateTimeZone('Asia/Manila')));
            $end = strtotime(Carbon::now('Asia/Manila'));
            $total = $end - $start; 
            if ($total > 60) {
                // min
                $time = round($total/60) . ' minute(s) ago';
                // hr
                if ($total/60 > 60) {
                    $time = round(($total/60)/60) . ' hour(s) ago';
                }
                // d
                if ((($total/60)/60) > 24) {
                    $time = round((($total/60)/60)/24) . ' day(s) ago';
                }
                // w
                if (((($total/60)/60)/24) > 7) {
                    $time = round(((($total/60)/60)/24)/7) . ' week(s) ago';
                }
                // m
                if ((((($total/60)/60)/24)/7) > 4) {
                    $time = round((((($total/60)/60)/24)/7)/4) . ' month(s) ago';
                }
            } else {
                $time = ($total) . ' second(s) ago';
            }

            array_push($data, [
                'id' => $message->id,
                'name' => $message->name,
                'email' => $message->email,
                'message' => strlen($message->message) > 30 ? substr($message->message,0,30).'...' : $message->message,
                'time' => $time,
                'isread' => $message->isRead,
            ]);
        }
        return response()->json(['messages' => $data, 'countUnRead' => ContactUs::where('isRead', false)->get()->count()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contact = new ContactUs();
        $contact->fill($request->all());

        if ($contact->save()) {
            $data = [
                'contact' => $contact,
            ];
            Mail::send('emails.send-contact-us', $data, function ($message) {
                $message->to('xailamoste@gmail.com')->subject('Contact Us');
            });

            return response()->json(['msg' => '<b>' . $contact->name . '</b> successfuly added!', 'success' => true]);
        } else {
            return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContactUs  $contactUs
     * @return \Illuminate\Http\Response
     */
    public function show(ContactUs $contactUs)
    {
        if ($contactUs->isRead != 1) {
            $contactUs->isRead = true;
            $contactUs->save();
        }
        return response()->json(['message' => $contactUs]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContactUs  $contactUs
     * @return \Illuminate\Http\Response
     */
    public function edit(ContactUs $contactUs)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContactUs  $contactUs
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContactUs $contactUs)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContactUs  $contactUs
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContactUs $contactUs)
    {
        //
    }
}
