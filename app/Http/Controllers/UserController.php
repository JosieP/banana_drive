<?php

namespace App\Http\Controllers;

use \App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function reset(Request $request, $id)
    {
        $user = User::find($id);
        $user->password = bcrypt($request->input('password'));

        if ($user->save()) {
            return response()->json(['msg' => '<b>' . $user->name . '</b> password successfuly updated!', 'success' => true]);
        } else {
            return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
        }
    }
}
