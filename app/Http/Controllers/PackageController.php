<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PackageController extends Controller
{
    public function get_packages_html()
    {
        $tools = \App\Package::where('type', 'tools')->where('status', 'available')->get();
        $menus = \App\Package::where('type', 'menu')->where('status', 'available')->get();
        $data = [];
        $packages = [];
        foreach ($tools as $tool) {
            array_push($packages, [
                'id' => $tool->id,
                'name' => $tool->name,
                'price' => $tool->price,
                'description' => $tool->description,
                'type' => $tool->type,
            ]);
        }
        foreach ($menus as $menu) {
            array_push($packages, [
                'id' => $menu->id,
                'name' => $menu->name,
                'price' => $menu->price,
                'lists' => $this->get_lists($menu->id),
                'description' => '<ul>' . $this->lists($menu) . '</ul><p style="color:red;">Note:The entree(s) can be changed depend on your likes, but the number of entree(s) will remain.</p>',
                'type' => $menu->type,
            ]);
        }
        foreach ($packages as $package) {
            array_push($data, 
                            '
                            <div class="col-6">
                                <div class="media d-block mb-4 text-center site-media border-0">
                                    <div class="media-body p-md-5 p-4 description">
                                    <h5 class="text-primary">P ' . $package['price'] . '</h5>
                                    <h5 class="mt-0 h4">' . $package['name'] . '</h5>
                                    <p class="mb-4">' . $package['description'] . '</p>
                                    </div>
                                </div>
                            </div>
                            '
            );
        }

        return response()->json(['packages' => $data]);
    }

    public function get_packages()
    {
        $tools = \App\Package::where('type', 'tools')->where('status', 'available')->get();
        $menus = \App\Package::where('type', 'menu')->where('status', 'available')->get();
        $data = [];
        $data1 = [];
        foreach ($tools as $tool) {
            array_push($data1, [
                'id' => $tool->id,
                'name' => $tool->name,
                'price' => $tool->price,
                'description' => $tool->description,
                'type' => $tool->type,
            ]);
        }
        foreach ($menus as $menu) {
            array_push($data, [
                'id' => $menu->id,
                'name' => $menu->name,
                'price' => $menu->price,
                'lists' => $this->get_lists($menu->id),
                'description' => '<ul>' . $this->lists($menu) . '</ul><p style="color:red;">Note:The entree(s) can be changed depend on your likes, but the number of entree(s) will remain.</p>',
                'type' => $menu->type,
            ]);
        }
        return response()->json(['tools' => $data1, 'menus' => $data]);
    }

    public function get_lists($id)
    {
        $lists = \App\PackageList::where('package_id', $id)->get();
        $data = [];
        foreach ($lists as $list) {
            array_push($data, [
                'id' => $list->id,
                'category_id' => $list->category_id,
                'value' => \App\FoodDrink::where('id', $list->food_id)->first(),
                'category_name' => \App\Category::where('id', $list->category_id)->first()->name,
            ]);
        }

        return $data;
    }

    public function get_services()
    {
        return response()->json(['services' => \App\Service::all()]);
    }

    public function lists($menu) {
        $lists = \App\PackageList::where('package_id', $menu->id)->get();
        $str = '';
        foreach ($lists as $list) {
            $str .= '<li>' . \App\Category::find($list->category_id)->name . '      -       ' . \App\FoodDrink::find($list->food_id)->name .'</li>';
        }

        return $str;
    }
}
