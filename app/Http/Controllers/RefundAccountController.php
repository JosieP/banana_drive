<?php

namespace App\Http\Controllers;

use Auth;
use App\RefundAccount;
use Illuminate\Http\Request;

class RefundAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware(['auth:admin'])->except('index', 'get_my_account');
        $this->middleware('is_admin')->except('index', 'get_my_account');
    }

    public function index()
    {
        return view('private.refund');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RefundAccount  $refundAccount
     * @return \Illuminate\Http\Response
     */
    public function show(RefundAccount $refundAccount)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RefundAccount  $refundAccount
     * @return \Illuminate\Http\Response
     */
    public function edit(RefundAccount $refundAccount)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RefundAccount  $refundAccount
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RefundAccount $refundAccount)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RefundAccount  $refundAccount
     * @return \Illuminate\Http\Response
     */
    public function destroy(RefundAccount $refundAccount)
    {
        //
    }

    public function get_my_account()
    {
        return response()->json(['account' => \App\RefundAccount::where('user_id', Auth::user()->id)->first()]);
    }
    public function get_refunds()
    {
        $refunds = \App\Payment::where('type', 'refund')->get();
        $data = [];
        foreach ($refunds as $refund) {
            array_push($data, [
                'id' => $refund->id,
                'amount' => $refund->amount,
                'description' => $refund->description,
                'title' => \App\Reservation::find($refund->reservation_id)->title,
                'status' => $refund->status,
                'filename' => $refund->filename,
                'url' => $refund->url,
                'status' => $refund->status,
                'type' => $refund->type,
                'date' => $refund->payment_date,
                'refference_no' => $refund->refference_no,
                'account' => \App\RefundAccount::where('user_id', \App\Reservation::find($refund->reservation_id)->user_id)->first()
            ]);
        }
        return response()->json(['refunds' => $data]);
    }
}
