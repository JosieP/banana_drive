<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\User;
use App\UserDetails;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware(['auth']);
    // }

    public function create()
    {
        if (Auth::check()) {
            return view('public.reservation');
        }
        return redirect('home');
    }
    public function get_user()
    {
        if (Auth::check()) {
            $user = Auth::user();
            $details = \App\UserDetails::where('user_id', $user->id)->first();
            $history = [];
        } else {
            $user = null;
            $details = null;
        }

        return response()->json(['user' => $user, 'details' => $details]);
    }

    public function get_admin()
    {
        $admin = \App\Admin::find(1);
        $admin->contact_number = substr($admin->contact_number, 3);

        return response()->json(['admin' => $admin]);
    }

    public function update_user(Request $request, User $user)
    {
        $user->fill($request->all());
        if ($user->save()) {
            \App\History::create([
                'user_id' => Auth::user()->id,
                'reservation_id' => null,
                'tag' => 'update-user',
            ]);
            return response()->json(['msg' => '<b>' . Auth::user()->firstname . '</b> successfuly added!', 'success' => true]);
        } else {
            return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
        }
    }

    public function update_user_details(Request $request, UserDetails $user_detail)
    {
        $name = User::select(DB::raw('CONCAT(firstname , " " , lastname) as name'))->where('id', $user_detail->id)->pluck('name')->first();

        try {
            $user_detail->fill($request->all());
            if ($user_detail->save()) {
                \App\History::create([
                    'user_id' => Auth::user()->id,
                    'reservation_id' => null,
                    'tag' => 'update-user-details',
                ]);
                return response()->json(['msg' => '<b>' . $name . '</b> successfuly added!', 'success' => true]);
            } 
        } catch (\Throwable $th) {
            \Log::info($th);
            return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
        }
    }

    public function save_details(Request $request)
    {
        $details = new \App\UserDetails();
        $details->fill($request->all());
        $details->user_id = Auth::user()->id;

        if ($details->save()) {
            return response()->json(['msg' => '<b>' . Auth::user()->firstname . '</b> successfuly added!', 'success' => true]);
        } else {
            return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
        }

    }
}
