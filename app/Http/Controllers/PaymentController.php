<?php

namespace App\Http\Controllers;

use Auth;
use Storage;
use App\Payment;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rs = \App\Reservation::where('user_id', Auth::user()->id)->pluck('id');
        $data1 = [];
        foreach ($rs as $r) {
            // $ps = Payment::where('reservation_id', $r->id)
            array_push($data1, Payment::where('reservation_id', $r)->get());
        }
        $dt = [];
        foreach ($data1 as $d) {
            foreach ($d as $e) {
                array_push($dt, $e);
            }
        }
        // return $dt;
        // $ps = Payment::where('reservation_id', \App\Reservation::where('user_id', Auth::user()->id)->get())->get();
        $data = [];
        foreach ($dt as $p) {
            // $date = \App\Reservation::find($p->reservation_id)->actual_date;
            array_push($data, [
                'id' => $p->id,
                'date' => $p->payment_date,
                'refference_no' => $p->refference_no,
                'date' => $p->payment_date,
                'filename' => $p->filename,
                'url' => $p->url,
                'type'=> $p->type,
                'amount' => $p->amount,
                'status' => $p->status,
                'reservation_id' => $p->reservation_id,
                'refund' => \App\RefundAccount::find(Auth::user()->id),
                'title' => \App\Reservation::find($p->reservation_id)->title,
                'rstatus' => \App\Reservation::find($p->reservation_id)->status
            ]);
        }

        return response()->json(['payments' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function reservation_fee(Request $request)
    {
        // Payment
        $reservation = \App\Reservation::find($request->input('reservation_id'));

        $file = \Request::file('file');
        $payment = new Payment();

        if (\Request::has('file')) {
            $contents = file_get_contents($file);
            $payment->url = 'payment-' . $payment->id . mt_rand() . '.' . $file->getClientOriginalExtension();
            $payment->filename .= $file->getClientOriginalName();
            $path = 'payments/' . $payment->url;
            Storage::disk('public')->put($path, $contents);
        }

        $payment->reservation_id = $request->input('reservation_id');
        $payment->refference_no = $request->input('refference_no');
        $payment->payment_date = $request->input('payment_date');
        $payment->amount = $request->input('amount');
        $payment->description = $request->input('description');
        // $payment->save();
        if ($payment->save()) {
            return response()->json(['success' => true]);
            // $reservation->amount_paid = $reservation->amount_paid + $request->input('amount');
            // $reservation->status = 'pending';
            // $reservation->save();
        }
        return response()->json(['success' => false]);
    }
    public function store(Request $request)
    {
        $file = \Request::file('file');
        $payment = new Payment();
        $reservation = \App\Reservation::find($request->input('reservation_id'));
        try {
            if ($file != null) {
                if ($request->input('amount') < ($reservation->amount * .75)) {
                    return response()->json(['msg' => 'Your downpayment amount is less than ' . $reservation->amount * .75 . '(75%).', 'success' => false]);
                }
                $contents = file_get_contents($file);
                $payment->url = 'payment-' . $payment->id . mt_rand() . '.' . $file->getClientOriginalExtension();
                $payment->filename .= $file->getClientOriginalName();
                $path = 'payments/' . $payment->url;
                Storage::disk('public')->put($path, $contents);

                $payment->reservation_id = $request->input('reservation_id');
                $payment->refference_no = $request->input('refference_no');
                $payment->payment_date = $request->input('payment_date');
                $payment->amount = $request->input('amount');
                $payment->description = $request->input('description');

                if ($payment->save()) {
                    $reservation->amount_paid = $reservation->amount_paid + $request->input('amount');
                    $reservation->status = 'pending';
                    $reservation->save();

                    \App\Notification::create([
                        'sender_id' => Auth::user()->id,
                        'receiver_id' => 1,
                        'reservation_id' => $reservation->id,
                        'tag' => 'create-payment',
                        'isRead' => false
                    ]);
                    \App\History::create([
                        'user_id' => Auth::user()->id,
                        'reservation_id' => $reservation->id,
                        'tag' => 'create-payment',
                    ]);

                    \App\StatusLog::create([
                        'user_id' => Auth::user()->id,
                        'reservation_id' => $reservation->id,
                        'tag' => 'create-payment',
                        'status' => 'pending'
                    ]);

                    return response()->json(['msg' => '<b>' . $reservation->title . '</b> successfuly added a payment!', 'success' => true]);
                } else {
                    return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
                }
            } else {
                return response()->json(['msg' => 'Insert image/screenshot for the proof.', 'success' => false]);
            }
        } catch (\Throwable $th) {
            \Log::info($th);
            return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function show(Payment $payment)
    {
        $payment->title = \App\Reservation::find($payment->reservation_id)->title;

        return response()->json(['payment' => $payment]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function edit(Payment $payment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function add_request_payment(Request $request)
    {
        $reservation = \App\Reservation::find($request->input('reservation_id'));

        $payment = new Payment();
        $payment->fill($request->all());

        if ($payment->save()) {
            
            $reservation->amount_paid = $reservation->amount_paid + $request->input('amount');
            $reservation->save();

            \App\History::create([
                'user_id' => $reservation->user_id,
                'reservation_id' => $reservation->id,
                'tag' => 'add_request-payment',
                'comment' => null
            ]);
            return response()->json(['msg' => '<b>' . $reservation->title . '</b> successfuly request a payment!', 'success' => true]);
        } else {
            return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
        }
    }
    public function pupdate(Request $request, $id)
    {   
        try {
            $reservation = \App\Reservation::find($request->input('reservation_id'));
            if ($request->input('methodss') == 'send:payment') {
                $reservation->amount_paid = $reservation->amount_paid + $request->input('amount');
                $payment = new Payment();
            } else {
                $payment = Payment::find($id);
                $reservation->amount_paid = $reservation->amount_paid + $request->input('amount');
            }
            $file = \Request::file('file');
            
            // $payment->fill($request->all());
            
            $payment->reservation_id = $request->input('reservation_id');
            $payment->refference_no = $request->input('refference_no');
            $payment->payment_date = $request->input('payment_date');
            $payment->amount = $request->input('amount');
            $payment->type = $request->input('type');
            $payment->status = $request->input('status');
            $payment->description = $request->input('description');

            $contents = file_get_contents($file);
            $payment->url = 'payment-' . $payment->id . mt_rand() . '.' . $file->getClientOriginalExtension();
            $payment->filename .= $file->getClientOriginalName();
            $path = 'payments/' . $payment->url;
            Storage::disk('public')->put($path, $contents);

            if ($payment->save()) {
                if (\App\RefundAccount::where('user_id', $reservation->user_id)->first() == null) {
                    \App\RefundAccount::create([
                        'user_id' => Auth::user()->id,
                        'type' => $request->input('rtype'),
                        'name' => $request->input('name'),
                        'account_number' => $request->input('account_number'),
                    ]);
                }
                // $reservation->amount_paid = $reservation->amount_paid + $request->input('amount');
                $ps = Payment::where('reservation_id', $reservation->id)->get();
            
                // if ($request->input('amount') >= ($reservation->amount * .75)) {
                    if ($reservation->status == 'pending' || $reservation->status == 'waiting') {
                        if ($this->check_unpaid($ps)) {
                            $reservation->status = 'waiting';
                        } else {
                            $reservation->status = 'pending';
                        }
                    }
                // }
                $reservation->save();

                \App\StatusLog::create([
                    'user_id' => Auth::user()->id,
                    'reservation_id' => $reservation->id,
                    'tag' => 'send-payment',
                    'status' => 'completed'
                ]);
                
                \App\History::create([
                    'user_id' => $reservation->user_id,
                    'reservation_id' => $reservation->id,
                    'tag' => 'send-payment',
                    'comment' => null
                ]);
                
                if (app('App\Http\Controllers\ReservationController')->api() != null) {
                    \Telnyx\Telnyx::setApiKey(app('App\Http\Controllers\ReservationController')->api());
    
                    $new_message = \Telnyx\Message::Create([
                                        'from' => '+17733606711', 
                                        'to' => \App\Admin::find(1)->contact_number, 
                                        'text' => 'Banana Drive Catering : Client send a payment on ' . $reservation->title . ' reservation. Please check the amount before you approved.'
                                    ]);
                }

                return response()->json(['msg' => '<b>' . $reservation->title . '</b> successfuly send a payment!', 'success' => true]);
            } else {
                return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
            }
        } catch (\Throwable $th) {
            //throw $th;
            \Log::info($th);
        }
    }

    public function check_unpaid($ps)
    {
        foreach ($ps as $p) {
            if ($p->type == 'unpaid') {
                return true;
            }
        }
        return false;
    }

    public function update(Request $request, Payment $payment)
    {
        $reservation = \App\Reservation::find($payment->reservation_id);
        $payment->fill($request->all());

        if ($payment->save()) {
            
            $ps = Payment::where('reservation_id', $reservation->id)->get();
            if ($this->check_unpaid($ps)) {
                $reservation->status = 'waiting';
            } else {
                $reservation->status = 'approved';
            }
            $reservation->save();

            \App\StatusLog::create([
                'reservation_id' => $reservation->id,
                'tag' => 'update-status-payment',
                'status' => 'completed'
            ]);

            \App\History::create([
                'user_id' => $reservation->user_id,
                'reservation_id' => $reservation->id,
                'tag' => 'admin-update-reservation-fee',
                'comment' => null
            ]);
            
            if (app('App\Http\Controllers\ReservationController')->api() != null) {
                \Telnyx\Telnyx::setApiKey(app('App\Http\Controllers\ReservationController')->api());
                
                $number = \App\UserDetails::where('user_id', $reservation->user_id)->first();
    
                $new_message = \Telnyx\Message::Create([
                                    'from' => '+17733606711', 
                                    'to' => '+63' . $reservation->contact_number,
                                    'text' => 'Banana Drive Catering : Admin accepted your payment on ' . $reservation->title . ' reservation. Please check your reservation.'
                                ]);
            }

            return response()->json(['msg' => '<b>' . $reservation->title . '</b> successfuly updated!', 'success' => true]);
        } else {
            return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payment $payment)
    {
        //
    }
    public function refund_payment(Payment $payment, Request $request)
    {
        $file = \Request::file('file');
        $r = \App\Reservation::find($request->input('reservation_id'));

        $contents = file_get_contents($file);
        $payment->url = 'payment-' . $payment->id . mt_rand() . '.' . $file->getClientOriginalExtension();
        $payment->filename .= $file->getClientOriginalName();
        $path = 'payments/' . $payment->url;
        Storage::disk('public')->put($path, $contents);

        $payment->refference_no = $request->input('refference_no');
        $payment->payment_date = $request->input('payment_date');
        $payment->description = $request->input('notes');
        $payment->type = 'refund';
        $payment->status = 'pending';

        if ($payment->save()) {

            \App\StatusLog::create([
                'reservation_id' => $request->input('reservation_id'),
                'tag' => 'refund-payment',
                'status' => 'cancelling'
            ]);

            \App\History::create([
                'user_id' => $r->user_id,
                'reservation_id' => $r->id,
                'tag' => 'admin-send-payment',
                'comment' => null
            ]);
            
            if (app('App\Http\Controllers\ReservationController')->api() != null) {
                \Telnyx\Telnyx::setApiKey(app('App\Http\Controllers\ReservationController')->api());
                
                $number = \App\UserDetails::where('user_id', $r->user_id)->first();
    
                $new_message = \Telnyx\Message::Create([
                                    'from' => '+17733606711', 
                                    'to' => '+63' . $r->contact_number,
                                    'text' => 'Banana Drive Catering : Admin send a payment for the refund on ' . $r->title . ' reservation. Please check the amount before aprroved.'
                                ]);
            }

            return response()->json(['msg' => 'Payment successfuly updated!', 'success' => true]);
        } else {
            return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
        }
    }
    public function cancel_payment(Payment $payment, Request $request)
    {
        $r = \App\Reservation::find($payment->reservation_id);
        if ($request->input('reason') != null) {
            $payment->description = $request->input('reason');
            $payment->status = 'waiting';
            $r->status = 'cancelling';
            $body = 'Banana Drive Catering : Client dicline your payment on ' . $r->title . ' reservation. Please check the reason of the client.';
        } else {
            $payment->status = 'cancelled';
            $r->status = 'cancelled';
            $body = 'Banana Drive Catering : Client accepted your payment on ' . $r->title . ' reservation.The status of the reservation is cancelled.';
        }

        if ($payment->save()) {
            $r->save();

            \App\StatusLog::create([
                'reservation_id' => $r->id,
                'tag' => 'refund-payment-complete',
                'status' => 'cancelled'
            ]);

            \App\History::create([
                'user_id' => $r->user_id,
                'reservation_id' => $r->id,
                'tag' => 'refund-payment-complete',
                'comment' => null
            ]);

            if (app('App\Http\Controllers\ReservationController')->api() != null) {
                \Telnyx\Telnyx::setApiKey(app('App\Http\Controllers\ReservationController')->api());
                
                $number = \App\UserDetails::where('user_id', $r->user_id)->first();
    
                $new_message = \Telnyx\Message::Create([
                                    'from' => '+17733606711', 
                                    'to' => \App\Admin::find(1)->contact_number, 
                                    'text' => $body
                                ]);
            }

            return response()->json(['msg' => 'Payment refund successfuly complete!', 'success' => true]);
        } else {
            return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
        }

    }

    public function admin_send_payment(Request $request)
    {
        try {
            $reservation = \App\Reservation::find($request->input('reservation_id'));
            $reservation->amount_paid = $reservation->amount_paid + $request->input('amount');
                
            $payment = new Payment();
                
            $file = \Request::file('file');
            $payment->fill($request->all());

            $contents = file_get_contents($file);
            $payment->url = 'payment-' . $payment->id . mt_rand() . '.' . $file->getClientOriginalExtension();
            $payment->filename .= $file->getClientOriginalName();
            $path = 'payments/' . $payment->url;
            Storage::disk('public')->put($path, $contents);

            if ($payment->save()) {
                $reservation->save();
                return response()->json(['msg' => 'Payment successfuly added!', 'success' => true]);
            } else {
                return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
            }
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public function payment_reservation(Request $request)
    {
        $payments = Payment::where('reservation_id', $request->input('id'))->get();

        $amount = 0;

        foreach ($payments as $payment) {
            $amount += $payment->amount;
        }
        return response()->json(['paid_amount' => $amount]);
    }
}
