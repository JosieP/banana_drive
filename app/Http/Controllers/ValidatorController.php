<?php

namespace App\Http\Controllers;

use App\User;
use App\Service;
use App\Material;
use App\Category;
use App\FoodDrink;
use App\Province;
use Illuminate\Http\Request;

class ValidatorController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware(['auth:admin']);
    // }

    public function unique(Request $request)
    {
    	$model = $request->input('model');
    	$isEdit = $request->has('id');
    	$value = $request->input('value');

    	if ($model == 'users')
	    {
            $admins = \App\Admin::select('id', 'firstname', 'lastname', 'email', 'type', 'created_at');
        
	    	if (!$isEdit)
		    {
		    	$result = User::where('email', $value)->first();
		    } else {
				$result = User::where('email', $value)->where('id', '<>', $request->input('id'))->first();
		    }

            return response()->json(['valid' => $result === null]);
	    } else if ($model == 'categories')
	    {
	    	if (!$isEdit)
		    {
		    	$result = Category::where('name', $value)->first();
		    } else {
				$result = Category::where('name', $value)->where('id', '<>', $request->input('id'))->first();
		    }

            return response()->json(['valid' => $result === null]);
	    } else if ($model == 'foods_drinks')
	    {
	    	if (!$isEdit)
		    {
		    	$result = FoodDrink::where('name', $value)->first();
		    } else {
				$result = FoodDrink::where('name', $value)->where('id', '<>', $request->input('id'))->first();
		    }

            return response()->json(['valid' => $result === null]);
	    } else if ($model == 'materials')
	    {
	    	if (!$isEdit)
		    {
		    	$result = Material::where('name', $value)->first();
		    } else {
				$result = Material::where('name', $value)->where('id', '<>', $request->input('id'))->first();
		    }

            return response()->json(['valid' => $result === null]);
	    } else if ($model == 'services')
	    {
	    	if (!$isEdit)
		    {
		    	$result = Service::where('name', $value)->first();
		    } else {
				$result = Service::where('name', $value)->where('id', '<>', $request->input('id'))->first();
		    }

            return response()->json(['valid' => $result === null]);
	    } else if ($model == 'provinces')
	    {
	    	if (!$isEdit)
		    {
		    	$result = Province::where('name', $value)->first();
		    } else {
				$result = Province::where('name', $value)->where('id', '<>', $request->input('id'))->first();
		    }

            return response()->json(['valid' => $result === null]);
	    } 
    }
}
