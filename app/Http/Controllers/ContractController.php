<?php

namespace App\Http\Controllers;

use App;
use Auth;
use Storage;
use App\Contract;
use Illuminate\Http\Request;

class ContractController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_package($id)
    {
        $p = \App\Package::find($id);

        if ($p->type == 'menu') {
            $p->description = '<ul>' . $this->lists($p) . '</ul>';
        } else {
            $p->description = $p->description;
        }
        return $p;
    }

    public function lists($menu) {
        $lists = \App\PackageList::where('package_id', $menu->id)->get();
        $str = '';
        foreach ($lists as $list) {
            $str .= '<li>' . \App\Category::find($list->category_id)->name . '      -       ' . \App\FoodDrink::find($list->food_id)->name .'</li>';
        }

        return $str;
    }

    public function index(Request $request, $id)
    {
        $reservation = \App\Reservation::find($id);
        // $reservation->packages = $this->get_packages($reservation->id);
        $reservation_packages = \App\ReservationPackage::where('reservation_id', $reservation->id)->get();
        $data1 = [];
        foreach ($reservation_packages as $p) {
            array_push($data1, [
                "id" => $p->id,
                "reservation_id" => $p->reservation_id,
                "packages" => $this->get_package($p->package_id),
                "quantity" => $p->quantity,
                "price" => $p->price,
            ]);
        }
        $user = \App\User::find($reservation->user_id);
        $municipality = \App\Province::where('id', $reservation->municipality_id)->first();
        $data = [
            'name' => $user->firstname . ' ' . $user->lastname,
            'address' => \App\UserDetails::where('user_id',$user->id)->first()->address,
            'contact' => $request->input('contact'),
            'date_reserve' => date('F d, Y', strtotime($reservation->created_at)),
            'date_function' => date('F d, Y', strtotime($reservation->start_actual_date)),
            'nature' => $request->input('event'),
            'motif' => 'none',
            'time' => date('h:m a', strtotime($reservation->start_actual_date)),
            'venue' => $reservation->address . ', ' . $municipality->name . ', Bohol',
            'rate_person' => '120',
            'noperson' => '100',
            'reserve' => 'sdf',
            'reservation' => $reservation,
            'reservation_packages' => $data1
        ];
        $pdf = App::make('dompdf.wrapper');
        $pdf = \PDF::loadView('pdf.contract', $data);
        return $pdf->stream();
        // $file = $pdf->render();
        // $output = $file->output();

        // $contents = file_get_contents($file);
        // // $payment->url = 'payment-' . $user->id . mt_rand() . '.' . $file->getClientOriginalExtension();
        // // $payment->filename .= $file->getClientOriginalName();
        // $path = 'contracts/download.pdf';
        // Storage::disk('public')->put($path, $contents);
        // return;
        // $contract = new Contract();
        // $contract->reservation_id = $id;
        // $contract->filename = 'contract-summary.pdf';
        // $contract->url = 'contract-summary-' . $id . mt_rand() . '.pdf';
        // $contract->save();

        // return $pdf->save(public_path() . '\storage\contracts')->stream('download.pdf');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function show(Contract $contract)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function edit(Contract $contract)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contract $contract)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contract $contract)
    {
        //
    }
}
