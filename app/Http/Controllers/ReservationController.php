<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use DateTimeZone;
use Carbon\Carbon;
use App\Reservation;
use \Carbon\CarbonPeriod;
use Illuminate\Http\Request;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function api()
    {
        // $api = 'KEY01745938153EAEA1D088D3E7D42278D6_7jsmZcAOYeRW3eXMR8SHOz';
        $api = null;
        return $api;
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function show(Reservation $reservation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function edit(Reservation $reservation)
    {
        return view('public.edit-reservation');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reservation $reservation)
    {
        try {
            DB::beginTransaction();
            $reservation->user_id = Auth::user()->id;
            $reservation->service_id = $request->input('service_id');
            $reservation->municipality_id = $request->input('municipality_id');
            $reservation->barangay_id = $request->input('barangay_id');
            $reservation->address = $request->input('address');
            $reservation->start_actual_date = date('Y-m-d', strtotime($request->input('start_actual_date')));
            $reservation->contact_number = $request->input('contact_number');
            $reservation->amount = $request->input('amount');
            $reservation->amount_paid = $reservation->amount_paid + $request->input('pamount');
            $reservation->delivery_fee = $request->input('delivery_fee');

            $reservation->save();
            // Save Reservation Package
            $rps = \App\ReservationPackage::where('reservation_id', $reservation->id)->get();
            foreach ($rps as $rpd) {
                $lists = \App\ReservationPackageList::where('reservation_package_id', $rpd->id)->delete();
                $rpd->delete();
            }
            foreach ($request->input('package') as $p) { 
                $rp = new \App\ReservationPackage();
                $rp->reservation_id = $reservation->id;
                $rp->package_id = $p['id'];
                $rp->quantity = $p['quantity'];
                $rp->time = $p['time'];
                $rp->price = $p['price'];
                $rp->save();
                // Save Reservation Package List
                if ($p['type'] == 'menu') {
                    foreach ($p['lists'] as $l) {
                        $rpl = new \App\ReservationPackageList();
                        $rpl->reservation_package_id = $rp->id;
                        $rpl->package_id = $rp->package_id;
                        $rpl->category_id = $l['category_id'];
                        $rpl->food_id = $l['value']['id'];
                        $rpl->save();
                    }
                }
            }
            if ($request->input('pamount') != 0) {
                \App\Payment::create([
                    'reservation_id' => $reservation->id,
                    'amount' => $request->input('pamount'),
                    'type' => 'unpaid',
                    'status' => 'waiting'
                ]);
            }
            \App\Notification::create([
                'sender_id' => Auth::user()->id,
                'receiver_id' => 1,
                'reservation_id' => $reservation->id,
                'tag' => 'update-reservation',
                'isRead' => false
            ]);
            \App\History::create([
                'user_id' => Auth::user()->id,
                'reservation_id' => $reservation->id,
                'tag' => 'update-reservation',
            ]);

            \App\StatusLog::create([
                'user_id' => Auth::user()->id,
                'reservation_id' => $reservation->id,
                'tag' => 'update-reservation',
                'status' => 'waiting'
            ]);

            if ($this->api() != null) {
                \Telnyx\Telnyx::setApiKey($this->api());
    
                $new_message = \Telnyx\Message::Create([
                                    'from' => '+17733606711', 
                                    'to' => \App\Admin::find(1)->contact_number, 
                                    'text' => 'Banana Drive Catering : Your reservation was updated. Please check on the website for the details.'
                                ]);
            }

            DB::commit();
            return response()->json(['msg' => 'Reservation update sent!', 'id' => $reservation->id, 'success' => true]);
        } catch (\Throwable $th) {
            \Log::info($th);
            DB::rollback();
            return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reservation $reservation)
    {
        //
    }

    public function get_reservation($id)
    {
        $r = \App\Reservation::find($id);
        $r->service_id = (int)$r->service_id;
        $r->municipality_id = (int)$r->municipality_id;
        $r->barangay_id = (int)$r->barangay_id;
        $r->actual_date = date('Y-m-d', strtotime($r->start_actual_date));
        $rps = \App\ReservationPackage::where('reservation_id', $r->id)->get();
        $data = [];
        foreach ($rps as $rp) {
            $package = \App\Package::find($rp->package_id);
                array_push($data, [
                    'id' => $package->id,
                    'name' => $package->name,
                    'price' => $package->price,
                    'quantity' => $rp->quantity,
                    'time' => date('H:m', strtotime($rp->time)),
                    'total' => $package->price * $rp->quantity,
                    'lists' => $this->get_lists($rp->id),
                    'description' => '<ul>' . $this->lists($rp) . '</ul><p style="color:red;">Note:The entree(s) can be changed depend on your likes, but the number of entree(s) will remain.</p>',
                    'type' => $package->type,
                ]);
        }
        $r->packages = $data;
        return response()->json(['reservation' => $r]);
    }

    public function get_lists($id)
    {
        $lists = \App\ReservationPackageList::where('reservation_package_id', $id)->get();
        $data = [];
        foreach ($lists as $list) {
            array_push($data, [
                'id' => $list->id,
                'category_id' => $list->category_id,
                'value' => \App\FoodDrink::where('id', $list->food_id)->first(),
                'category_name' => \App\Category::where('id', $list->category_id)->first()->name,
            ]);
        }

        return $data;
    }

    public function lists($menu) {
        $lists = \App\ReservationPackageList::where('reservation_package_id', $menu->id)->get();
        $str = '';
        foreach ($lists as $list) {
            $str .= '<li>' . \App\Category::find($list->category_id)->name . '      -       ' . \App\FoodDrink::find($list->food_id)->name .'</li>';
        }

        return $str;
    }

    public function cancel_reservation(Request $request, Reservation $reservation)
    {
        try {
            
            $ps = \App\Payment::where('reservation_id', $reservation->id)->delete();

            if ($request->input('isStatus')) {
                $reservation->status = 'cancelling';

                \App\Payment::create([
                    'reservation_id' => $reservation->id,
                    'amount' => $request->input('amount'),
                    'type' => 'refund',
                    'status' => 'waiting',
                    'description' => $request->input('description')
                ]);

                $r = \App\RefundAccount::firstOrNew([
                    'user_id' => Auth::user()->id,
                ]);
                $r->type = $request->input('rtype');
                $r->name = $request->input('name');
                $r->account_number = $request->input('account');
                $r->save();

            } else {
                $reservation->status = 'cancelled';

                \App\Payment::create([
                    'reservation_id' => $reservation->id,
                    'type' => 'unpaid',
                    'amount' => $reservation->amount_paid,
                    'status' => 'cancelled',
                    'description' => $request->input('description')
                ]);
            }

            if ($reservation->save()) {

                \App\Notification::create([
                    'sender_id' => Auth::user()->id,
                    'receiver_id' => 1,
                    'reservation_id' => $reservation->id,
                    'tag' => 'cancel-reservation',
                    'isRead' => false
                ]);
                \App\History::create([
                    'user_id' => Auth::user()->id,
                    'reservation_id' => $reservation->id,
                    'tag' => 'cancel-reservation',
                ]);

                if ($this->api() != null) {
                    \Telnyx\Telnyx::setApiKey($this->api());
        
                    $new_message = \Telnyx\Message::Create([
                                        'from' => '+17733606711', 
                                        'to' => \App\Admin::find(1)->contact_number, 
                                        'text' => 'Banana Drive Catering : Your reservation was cancelled. Please check on the website for the details.'
                                    ]);
                }

                return response()->json(['msg' => 'Successfully cancel!', 'success' => true]);
            } else {
                return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
            }
        } catch (\Throwable $th) {
            \Log::info($th);
        }

    }

    public function add_reservation(Request $request) {
        $reservation = new Reservation();
        try {
            DB::beginTransaction();
            $reservation->user_id = Auth::user()->id;
            $reservation->service_id = $request->input('service_id');
            $reservation->municipality_id = $request->input('municipality_id');
            $reservation->barangay_id = $request->input('barangay_id');
            $reservation->address = $request->input('address');
            $reservation->start_actual_date = date('Y-m-d H:i:s', strtotime($request->input('start_actual_date')));
            $reservation->contact_number = $request->input('contact_number');
            $reservation->amount = $request->input('amount');
            $reservation->delivery_fee = $request->input('delivery_fee');
            $reservation->status = 'waiting';

            $reservation->save();
            // Save Reservation Package
            foreach ($request->input('package') as $p) { 
                $rp = new \App\ReservationPackage();
                $rp->reservation_id = $reservation->id;
                $rp->package_id = $p['id'];
                $rp->quantity = $p['quantity'];
                $rp->time = $p['time'];
                $rp->price = $p['price'];
                $rp->save();
                // Save Reservation Package List
                if ($p['type'] == 'menu') {
                    foreach ($p['lists'] as $l) {
                        $rpl = new \App\ReservationPackageList();
                        $rpl->reservation_package_id = $rp->id;
                        $rpl->package_id = $rp->package_id;
                        $rpl->category_id = $l['category_id'];
                        $rpl->food_id = $l['value']['id'];
                        $rpl->save();
                    }
                }
            }
            \App\Payment::create([
                'reservation_id' => $reservation->id,
                'amount' => $request->input('pamount'),
                'type' => 'unpaid',
                'status' => 'waiting'
            ]);
            \App\Notification::create([
                'sender_id' => Auth::user()->id,
                'receiver_id' => 1,
                'reservation_id' => $reservation->id,
                'tag' => 'create-reservation',
                'isRead' => false
            ]);
            \App\History::create([
                'user_id' => Auth::user()->id,
                'reservation_id' => $reservation->id,
                'tag' => 'create-reservation',
            ]);

            \App\StatusLog::create([
                'user_id' => Auth::user()->id,
                'reservation_id' => $reservation->id,
                'tag' => 'create-reservation',
                'status' => 'waiting'
            ]);

            if ($this->api() != null) {
                \Telnyx\Telnyx::setApiKey($this->api());
    
                $new_message = \Telnyx\Message::Create([
                                    'from' => '+17733606711', 
                                    'to' => \App\Admin::find(1)->contact_number, 
                                    'text' =>  'Banana Drive Catering : Your reservation was created. Please check on the website for the details.'
                                ]);
            }

            DB::commit();
            return response()->json(['msg' => 'Reserved Sent!', 'id' => $reservation->id, 'success' => true]);
        } catch (\Throwable $th) {
            \Log::info($th);
            DB::rollback();
            return response()->json(['msg' => 'Failed to add data!', 'success' => false]);
        }

    }
    public function get_my_reservations()
    {
        $reservations = Reservation::where('user_id', Auth::user()->id)->get();
        $data = [];
        $data1 = [];
        $balance = 0.00;
        $paid = 0.00;
        foreach ($reservations as $reservation) {
            $end = strtotime(Carbon::parse($reservation->start_actual_date)->setTimezone(new DateTimeZone('Asia/Manila')));
            $start = strtotime(Carbon::now('Asia/Manila'));
            $total = $end - $start;
            $time = round((($total/60)/60)/24);

            if ($reservation->status == 'cancelled') {
                array_push($data1, [
                    'orig_id' => $reservation->id,
                    'id' => str_pad($reservation->id, 8, 0, STR_PAD_LEFT),
                    'date'  => date('M d, Y', strtotime($reservation->start_actual_date)),
                    'user_id' => $reservation->user_id,
                    'etime' => \App\ReservationPackage::where('reservation_id', $reservation->id)->first()->time,
                    'time' => $time,
                    'username' => \App\User::select(DB::raw('CONCAT(firstname , " " , lastname) as name'))->where('id', $reservation->user_id)->pluck('name')->first(),
                    'service_id' => $reservation->service_id,
                    'service_name' => \App\Service::where('id', $reservation->service_id)->first()->name,
                    'municipality_id' => $reservation->municipality_id,
                    'municipality_name' => \App\Province::where('id', $reservation->municipality_id)->first()->name,
                    'address' => ucwords($reservation->address),
                    'status' => $reservation->status,
                    'created_at' => $reservation->created_at,
                    'amount' => $reservation->amount,
                    'amount_paid' => $reservation->amount_paid,
                    'billing' => \App\Payment::where('reservation_id', $reservation->id)->get(),
                ]);
            } else {
                $balance = $balance + $reservation->amount;
                $paid = $paid + $reservation->amount_paid;
                // if (condition) {
                //     # code...
                // }
                array_push($data, [
                    'orig_id' => $reservation->id,
                    // 'lists' => $this->get_lists($reservation->id),
                    'id' => str_pad($reservation->id, 8, 0, STR_PAD_LEFT),
                    'date'  => date('M d, Y', strtotime($reservation->start_actual_date)),
                    'user_id' => $reservation->user_id,
                    'etime' => \App\ReservationPackage::where('reservation_id', $reservation->id)->first()->time,
                    'time' => $time,
                    'username' => \App\User::select(DB::raw('CONCAT(firstname , " " , lastname) as name'))->where('id', $reservation->user_id)->pluck('name')->first(),
                    'service_id' => $reservation->service_id,
                    'service_name' => \App\Service::where('id', $reservation->service_id)->first()->name,
                    'municipality_id' => $reservation->municipality_id,
                    'municipality_name' => \App\Province::where('id', $reservation->municipality_id)->first()->name,
                    'address' => ucwords($reservation->address),
                    'status' => $reservation->status,
                    'created_at' => $reservation->created_at,
                    'amount' => $reservation->amount,
                    'amount_paid' => $reservation->amount_paid,
                    'billing' => \App\Payment::where('reservation_id', $reservation->id)->get(),
                ]);
            }
        }

        return response()->json(['reservations' => $data, 'creservations' => $data1, 'balance' => $balance - $paid]);
    }

    public function date_checker(Request $request)
    {
        if ($request->input('id') != null) {
            $r = \App\Reservation::whereDate('start_actual_date', date('Y-m-d', strtotime($request->input('date'))))
                                ->where('user_id', Auth::user()->id)
                                ->where('status', '<>', 'cancelled')
                                ->where('status', '<>', 'cancelling')->get();
                                
        
        } else {
            $r = \App\Reservation::whereDate('start_actual_date', date('Y-m-d', strtotime($request->input('date'))))
                                    ->where('status', '<>', 'cancelling')->get();
        }
        
        if (count($r) < 1) {
            return response()->json(['check' => false]);
        }
        return response()->json(['check' => true]);
    }
    public function check_reservation()
    {
        $rs = Reservation::where('start_actual_date', '<', date('Y-m-d H:m:s', strtotime('3 days')))
                        ->where('status', 'waiting')
                        ->get();

        foreach ($rs as $r) {
            $r->status = 'cancelled';
            $r->save();
        }
        return;
    }

    public function get_reservation_date()
    {
        $reservations = Reservation::all();

        $background_colors = array('gray', 'red', 'orange', 'yellow', 'green', 'teal', 'blue', 'indigo', 'purple', 'pink');

        $data = [];

        for ($i=0; $i < count($reservations); $i++) { 
            if ($i == 0) {
                array_push($data, [
                    'dot' => true,
                    'dates' => [date('Y, m, d', strtotime($reservations[$i]->start_actual_date))],
                    'description' => \App\Service::find($reservations[$i]->service_id)->name,
                ]);
            } else {
                array_push($data, [
                    'key' => $i + 1,
                    'dot' => $background_colors[array_rand($background_colors)],
                    'dates' => [date('Y, m, d', strtotime($reservations[$i]->start_actual_date))],
                    'description' => \App\Service::find($reservations[$i]->service_id)->name,
                ]);
            }
        }

        return response()->json(['attributes' => $data]);
    }
}
