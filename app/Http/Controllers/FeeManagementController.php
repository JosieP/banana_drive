<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;

class FeeManagementController extends Controller
{
    public function index()
    {
        return view('reports.fee-management');
    }

    public function fee_management(Request $request)
    {
        if ($request->input('user') != null && $request->input('dateRange') != null) {
            $reservations = \App\Reservation::where('user_id', $request->input('user'))
                                            ->whereBetween(DB::raw('DATE(`created_at`)') , $request->input('dateRange'))
                                            ->get();
        } else {
            $reservations = \App\Reservation::all();
        }

        $data = [];
        foreach ($reservations as $reservation) {
            array_push($data, [
                'orig_id' => $reservation->id,
                'id' => str_pad($reservation->id, 8, 0, STR_PAD_LEFT),
                'title' => $reservation->title,
                'date' => $reservation->actual_date,
                'user_id' => $reservation->user_id,
                'username' => \App\User::select(DB::raw('CONCAT(firstname , " " , lastname) as name'))->where('id', $reservation->user_id)->pluck('name')->first(),
                'service_id' => $reservation->service_id,
                'service_name' => \App\Service::where('id', $reservation->service_id)->first()->name,
                'municipality_id' => $reservation->municipality_id,
                'municipality_name' => \App\Province::where('id', $reservation->municipality_id)->first()->name,
                'address' => $reservation->address,
                'status' => $reservation->status,
                'billing' => \App\Payment::where('reservation_id', $reservation->id)->first(),
                'delivery_fee' => $reservation->delivery_fee,
                'created_at' => $reservation->created_at,
            ]);
        }
        return response()->json(['reservations' => $data]);
    }
}
