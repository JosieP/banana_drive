<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    public function receipt($id)
    {
        $b = \App\Payment::find($id);
        $b->reservation = \App\Reservation::find($b->reservation_id);
        $data = [];
        foreach (\App\ReservationPackage::where('reservation_id', $b->reservation_id)->get() as $list) {
            $p = \App\Package::find($list->package_id);
            array_push($data, [
                'name' => $p->name,
                'price' => $list->price,
                'quantity' => $list->quantity,
            ]);
        }
        $b->reservation_lists = $data;
        $b->user = \App\User::find($b->reservation->user_id);
        $b->user_details = \App\UserDetails::where('user_id', $b->user->id)->first();
        // return $b;
        // return response()->json(['b' => $b]);
        return view('public.receipt')->with('b', $b);
    }

    public function generate_pdf($id)
    {
        $b = \App\Payment::find($id);
        $b->reservation = \App\Reservation::find($b->reservation_id);
        $data = [];
        foreach (\App\ReservationPackage::where('reservation_id', $b->reservation_id)->get() as $list) {
            $p = \App\Package::find($list->package_id);
            array_push($data, [
                'name' => $p->name,
                'price' => $list->price,
                'quantity' => $list->quantity,
            ]);
        }
        $b->reservation_lists = $data;
        $b->user = \App\User::find($b->reservation->user_id);
        $b->user_details = \App\UserDetails::find($b->user->id);

        $pdf = App::make('dompdf.wrapper');
        $pdf = \PDF::loadView('public.receipt', $b);
        return $pdf->stream();
    }
}
