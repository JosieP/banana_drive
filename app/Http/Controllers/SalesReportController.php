<?php

namespace App\Http\Controllers;

use DB;
use DateTime;
use DatePeriod;
use DateInterval;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SalesReportController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin']);
        $this->middleware('is_admin')->except('index');
    }

    public function index()
    {
        return view('reports.sales-report');
    }

    public function weekdate($weekNum)
    {
        $result = array();
        $datetime = new DateTime();
        $datetime->setISODate((int)$datetime->format('o'), $weekNum, 1);
        $interval = new DateInterval('P1D');
        $week = new DatePeriod($datetime, $interval, 6);

        foreach($week as $day){
            $result[] = $day->format('F d, Y');
        }
        return current($result) . ' - ' . end($result);
    }

    public function get_report(Request $request)
    {
        $reservations = \App\Reservation::whereYear('created_at', '=', '2020')
                                        ->where('status', 'approved')
                                        ->orWhere('status', 'completed')
                                        ->get()
                                        ->groupBy(function($d) use ($request) {
                                            if ($request->input('choice') == 'daily') {
                                                return Carbon::parse($d->created_at)->format('F d, Y');
                                            }
                                            if ($request->input('choice') == 'weekly') {
                                                $week = Carbon::parse($d->created_at)->format('W');
                                                return $this->weekdate($week);
                                            }
                                            if ($request->input('choice') == 'monthly') {
                                                return Carbon::parse($d->created_at)->format('F');
                                            }
                                            if ($request->input('choice') == 'yearly') {
                                                return Carbon::parse($d->created_at)->format('Y');
                                            }
                                        });

        $result = [];
        foreach ($reservations as $key => $reservation) {
            $data = [];
            $total = 0;
            $total_fee = 0;
            foreach ($reservation as $r) {
                $total += $r->amount;
                $total_fee += $r->delivery_fee;
                array_push($data, [
                    'title' => $r->title,
                    'amount' => $r->amount,
                    'username' => \App\User::select(DB::raw('CONCAT(firstname , " " , lastname) as name'))->where('id', $r->user_id)->pluck('name')->first(),
                    'date' => $r->start_actual_date,
                    'fee' => $r->delivery_fee,
                    'total' => $total,
                    'total_fee' => $total_fee,
                ]);
            }

            $result[$key] = $data;
        }
        return response()->json(['reservations' => $result]);
    }
}
