<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use DateTimeZone;
use Carbon\Carbon;
use App\Notification;
use App\Reservation;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware(['auth:admin'])->except('index');
        $this->middleware('is_admin')->except('index');
    }

    public function index()
    {
        if (Auth::guard('admin')->check()) {
            $ns = Notification::where('receiver_id', 1)->orderBy('id', 'DESC')->get();
            $data = [];
            foreach ($ns as $n) {
                $sender = \App\User::select(DB::raw('CONCAT(firstname , " " , lastname) as name'))->where('id', $n->sender_id)->pluck('name')->first();
                $start = strtotime($n->created_at->setTimezone(new DateTimeZone('Asia/Manila')));
                $end = strtotime(Carbon::now('Asia/Manila'));
                $total = $end - $start; 
                if ($total > 60) {
                    // min
                    $time = round($total/60) . ' minute(s) ago';
                    // hr
                    if ($total/60 > 60) {
                        $time = round(($total/60)/60) . ' hour(s) ago';
                    }
                    // d
                    if ((($total/60)/60) > 24) {
                        $time = round((($total/60)/60)/24) . ' day(s) ago';
                    }
                    // w
                    if (((($total/60)/60)/24) > 7) {
                        $time = round(((($total/60)/60)/24)/7) . ' week(s) ago';
                    }
                    // m
                    if ((((($total/60)/60)/24)/7) > 4) {
                        $time = round((((($total/60)/60)/24)/7)/4) . ' month(s) ago';
                    }
                } else {
                    $time = ($total) . ' second(s) ago';
                }
    
                if ($n->tag == 'create-reservation') {
                    array_push($data, [
                        'id' => $n->id,
                        'sender' => $sender,
                        'date' => date('Y-m-d', strtotime($n->created_at)),
                        'reservation_id' => $n->reservation_id,
                        'body' => '<b>' . \App\Reservation::find($n->reservation_id)->title . '</b> was created. Check the reservation details.',
                        'time' => $time,
                        'isread' => $n->isRead,
                    ]);
                }
                if ($n->tag == 'create-payment') {
                    array_push($data, [
                        'id' => $n->id,
                        'sender' => $sender,
                        'date' => date('Y-m-d', strtotime($n->created_at)),
                        'reservation_id' => $n->reservation_id,
                        'body' => 'You have a new payment reservation. Check the reservation for approval.',
                        'time' => $time,
                        'isread' => $n->isRead,
                    ]);
                }
                
                if ($n->tag == 'cancel-reservation') {
                    array_push($data, [
                        'id' => $n->id,
                        'sender' => $sender,
                        'date' => date('Y-m-d', strtotime($n->created_at)),
                        'reservation_id' => $n->reservation_id,
                        'body' => '<b>' . \App\Reservation::find($n->reservation_id)->title . '</b> reservation was cancelled. Please check the details.',
                        'time' => $time,
                        'isread' => $n->isRead,
                    ]);
                }
                if ($n->tag == 'update-reservation') {
                    array_push($data, [
                        'id' => $n->id,
                        'sender' => $sender,
                        'date' => date('Y-m-d', strtotime($n->created_at)),
                        'reservation_id' => $n->reservation_id,
                        'body' => '<b>' . \App\Reservation::find($n->reservation_id)->title . '</b> reservation was updated. Please check the details.',
                        'time' => $time,
                        'isread' => $n->isRead,
                    ]);
                }
            }
            return response()->json(['notifications' => $data, 'countUnRead' => Notification::where('isRead', false)->get()->count()]);
        } else {
            return response()->json(['notifications' => null, 'countUnRead' => 0]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function show(Reservation $notification)
    {
        return view('notifications.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function edit(Notification $notification)
    {
        $notification->isRead = true;
        $notification->save();
        $url = '/notifications/' . $notification->reservation_id;
        return redirect()->to($url);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Notification $notification)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function destroy(Notification $notification)
    {
        //
    }
}
