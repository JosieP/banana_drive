<?php

namespace App\Http\Controllers;

use App;
use Mail;
use Auth;
use App\Province;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth')->only('dashboard');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('public.home');
    }

    public function menu()
    {
        return view('public.menu');
    }
    public function dashboard()
    {
        if (Auth::check()) {
            return view('public.dashboard');
        }
        return redirect('home');
    }
    public function contact()
    {
        return view('public.contact');
    }

    public function about()
    {
        return view('public.about-us');
    }

    
    public function test()
    {
        $data = [
            'name' => 'Efrren Petarte',
            'address' => 'Cancatac, Corella, Bohol',
            'contact' => '09129937350',
            'date_reserve' => 'Oct. 18, 2020',
            'date_function' => 'Oct. 22, 2020',
            'nature' => 'Wedding',
            'motif' => 'none',
            'time' => '4:00 pm',
            'venue' => 'aa resort',
            'rate_person' => '120',
            'noperson' => '100',
            'reserve' => 'sdf'
        ];
        $pdf = App::make('dompdf.wrapper');
        // $pdf->loadHTML('<h1>Test</h1>');
        $pdf = \PDF::loadView('pdf.contract', $data);
        return $pdf->stream();
        // return response()->json(['success' => true]);
    }

    public function get_provinces_municipalities()
    {
        return response()->json(['provinces' => Province::all()]);
    }
    public function get_provinces()
    {
        return response()->json(['provinces' => Province::where('type', 'province')->get()]);
    }
    public function get_municipalities(Request $request)
    {
        $ms = Province::where('type', 'municipality')->where('province_id', $request->input('id'))->get();
        $data = [];
        foreach ($ms as $m) {
            array_push($data, [
                'id' => $m->id,
                'name' => ucwords($m->name),
                'price' => $m->price,
                'type' => $m->type,
                'province_id' => $m->province_id
            ]);
        }
        return response()->json(['municipalities' => $data]);
    }
    public function get_barangays(Request $request)
    {
        $bs = Province::where('type', 'barangay')->where('province_id', $request->input('id'))->get();
        $data = [];
        foreach ($bs as $b) {
            array_push($data, [
                'id' => $b->id,
                'name' => ucwords($b->name),
                'price' => $b->price,
                'type' => $b->type,
                'province_id' => $b->province_id
            ]);
        }

        return response()->json(['barangays' => $data]);
    }
}
