<?php

namespace App\Http\Controllers;

use App\Reservation;
use Illuminate\Http\Request;

class CalendarController extends Controller
{
    public function index()
    {
        return view('private.calendar');
    }

    public function get_calendar_date()
    {
        $reservations = Reservation::all();

        $class = array('bg-primary', 'bg-success', 'bg-info', 'bg-warning', 'bg-danger', 'bg-secondary', 'bg-dark');

        $data = [];

        for ($i=0; $i < count($reservations); $i++) { 
            if ($i == 0) {
                array_push($data, [
                    'key' => $i + 1,
                    'dates' => [date('Y, m, d', strtotime($reservations[$i]->start_actual_date))],
                    'customData' => [
                        'description' => \App\Service::find($reservations[$i]->service_id)->name,
                        'class' => $class[array_rand($class)]
                    ],
                ]);
            } else {
                array_push($data, [
                    'key' => $i + 1,
                    'dates' => [date('Y, m, d', strtotime($reservations[$i]->start_actual_date))],
                    'customData' => [
                        'description' => \App\Service::find($reservations[$i]->service_id)->name,
                        'class' => $class[array_rand($class)]
                    ],
                ]);
            }
        }

        return response()->json(['attributes' => $data]);
    }
}
