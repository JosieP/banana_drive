<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserDetails extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'user_id', 'gender', 'birthdate', 'status', 'address', 'contact'
    ];

    public function user()
    {
        return $this->hasOne(App\User::class);
    }
}
