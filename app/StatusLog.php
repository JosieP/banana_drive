<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusLog extends Model
{
    protected $fillable = ['user_id', 'reservation_id', 'status', 'tag', 'notes'];
}
