<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = ['sender_id', 'receiver_id', 'reservation_id', 'tag', 'isRead'];
}
