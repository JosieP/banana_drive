<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DisabledDates extends Model
{
    protected $fillable = ['d_date'];
}
