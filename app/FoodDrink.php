<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FoodDrink extends Model
{
    protected $fillable = ['name', 'category_id', 'sub_category_id', 'description', 'photo', 'filename'];
}
