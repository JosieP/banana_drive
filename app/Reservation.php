<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reservation extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'user_id', 
        'title', 
        'service_id', 
        'province_id', 
        'municipality_id', 
        'start_actual_date', 
        'delivery_fee', 
        'contact_number', 
        'amount', 
        'amount_paid', 
        'address', 
        'status',
        'barangay_id'
    ];
}
