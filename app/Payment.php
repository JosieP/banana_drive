<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = ['reservation_id', 'refference_no', 'payment_date', 'amount', 'filename', 'url', 'description', 'type', 'status'];
}
