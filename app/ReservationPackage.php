<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservationPackage extends Model
{
    protected $fillable = ['reservation_id', 'package_id', 'quantity', 'price'];
}
