<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendContactUs extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.send-contact-us')
                    ->from($this->data['email'], $this->data['name'])
                    ->cc($this->data['email'], $this->data['name'])
                    ->bcc($this->data['email'], $this->data['name'])
                    ->replyTo($this->data['email'], $this->data['name'])
                    ->subject('Contact Us')
                    ->with([ 'test_message' => $this->data['message'] ]);
        // return $this->markdown('emails.send-contact-us');
    }
}
