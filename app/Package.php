<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $fillable = ['name', 'description', 'type', 'price', 'package_type', 'status', 'food_max'];

    public function lists()
    {
        return $this->hasMany(PackageList::class);
    }
}
