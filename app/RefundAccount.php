<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefundAccount extends Model
{
    protected $fillable = ['user_id', 'account_number', 'name', 'type'];
}
