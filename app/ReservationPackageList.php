<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservationPackageList extends Model
{
    protected $fillable = ['reservation_package_id', 'package_id', 'category_id', 'food_id'];
}
