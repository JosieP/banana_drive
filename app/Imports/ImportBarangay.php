<?php

namespace App\Imports;

use App\Province;
use Maatwebsite\Excel\Concerns\ToModel;

class ImportBarangay implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return Province::firstOrNew([
            'name' => @$row[0],
            'province_id' => @$row[1],
            'price' => 0.00,
            'type' => @$row[2],
        ]);
    }
}
