<?php

use Illuminate\Database\Seeder;

class FoodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo 'Creating foods: ';
        $categories = \App\Category::all();

        foreach ($categories as $category) {
            if ($category->name == 'beef') {
                // BEEF
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Filipino Beef Steak',
                    'category_id' => $category->id,
                    'sub_category_id' => 1,
                    'description' => 'Filipino Beef Steak'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Meatballs',
                    'category_id' => $category->id,
                    'sub_category_id' => 1,
                    'description' => 'Meatballs'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Beef Steak with mushroom',
                    'category_id' => $category->id,
                    'sub_category_id' => 1,
                    'description' => 'Beef Steak with mushroom'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Beef Steak with broccoli',
                    'category_id' => $category->id,
                    'sub_category_id' => 1,
                    'description' => 'Beef Steak with broccoli'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Beef Caldereta',
                    'category_id' => $category->id,
                    'sub_category_id' => 1,
                    'description' => 'Beef Caldereta'
                ]);
                echo "OK!\n";
            } else if ($category->name == 'pork') {
                // PORK
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Pork Chinese Humba',
                    'category_id' => $category->id,
                    'sub_category_id' => 1,
                    'description' => 'Pork Chinese Humba'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Pork La Roca',
                    'category_id' => $category->id,
                    'sub_category_id' => 1,
                    'description' => 'Pork La Roca'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Pork Steak',
                    'category_id' => $category->id,
                    'sub_category_id' => 1,
                    'description' => 'Pork Steak'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Pork Menudo',
                    'category_id' => $category->id,
                    'sub_category_id' => 1,
                    'description' => 'Pork Menudo'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Classic Pork Adobo',
                    'category_id' => $category->id,
                    'sub_category_id' => 1,
                    'description' => 'Clasic Pork Adobo'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Porkchop Salciado',
                    'category_id' => $category->id,
                    'sub_category_id' => 1,
                    'description' => 'Porkchop Salciado'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Pork Sweet and Sour',
                    'category_id' => $category->id,
                    'sub_category_id' => 1,
                    'description' => 'Pork Sweet and Sour'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Tenderloin Steak Chinese Style',
                    'category_id' => $category->id,
                    'sub_category_id' => 1,
                    'description' => 'Tenderloin Steak Chinese Style'
                ]);
                echo "OK!\n";
            } else if ($category->name == 'vegetables') {
                // VEGETABLE
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Chopsuey',
                    'category_id' => $category->id,
                    'sub_category_id' => 1,
                    'description' => 'Chopsuey'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Chopsuey Special',
                    'category_id' => $category->id,
                    'sub_category_id' => 1,
                    'description' => 'Chopsuey Special'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Mixed Vegetables',
                    'category_id' => $category->id,
                    'sub_category_id' => 1,
                    'description' => 'Mixed Vegetables'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Chow Pat Chin Vegetables',
                    'category_id' => $category->id,
                    'sub_category_id' => 1,
                    'description' => 'Chow Pat Chin Vegetables'
                ]);
                echo "OK!\n";
            } else if ($category->name == 'dessert') {
                // DESSERT
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Sliced Fresh Fruits',
                    'category_id' => $category->id,
                    'description' => 'Sliced Fresh Fruits'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Choco Moist',
                    'category_id' => $category->id,
                    'description' => 'Choco Moist'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Macaroni Salad',
                    'category_id' => $category->id,
                    'description' => 'Macaroni Salad'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Fruit Salad',
                    'category_id' => $category->id,
                    'description' => 'Fruit Salad'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Macaroons',
                    'category_id' => $category->id,
                    'description' => 'Macaroons'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Leche Flan',
                    'category_id' => $category->id,
                    'description' => 'Leche Flan'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Maja Blanca',
                    'category_id' => $category->id,
                    'description' => 'Maja Blanca'
                ]);
                echo "OK!\n";
            } else if ($category->name == 'noodles or pasta') {
                // NOODLES OR PASTA
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Bam-I Special',
                    'category_id' => $category->id,
                    'sub_category_id' => 1,
                    'description' => 'Bam-I Special'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Bihon Guisado',
                    'category_id' => $category->id,
                    'sub_category_id' => 1,
                    'description' => 'Bihon Guisado'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Sotanghol Guisado',
                    'category_id' => $category->id,
                    'sub_category_id' => 1,
                    'description' => 'Sotanghol Guisado'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Spaghetti',
                    'category_id' => $category->id,
                    'sub_category_id' => 1,
                    'description' => 'Spaghetti'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Creamy Carbonara',
                    'category_id' => $category->id,
                    'sub_category_id' => 1,
                    'description' => 'Creamy Carbonara'
                ]);
                echo "OK!\n";
            } else if ($category->name == 'soup') {
                // SOUP
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Corn',
                    'category_id' => $category->id,
                    'description' => 'Corn'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Cream of Mushroom',
                    'category_id' => $category->id,
                    'description' => 'Cream of Mushroom'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Cream of Asparagus',
                    'category_id' => $category->id,
                    'description' => 'Cream of Asparagus'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Oriental Seafood Bouillabaisse',
                    'category_id' => $category->id,
                    'description' => 'Oriental Seafood Bouillabaisse'
                ]);
                echo "OK!\n";
            } else if ($category->name == 'chicken') {
                // CHICKEN
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Buttered Chicken',
                    'category_id' => $category->id,
                    'sub_category_id' => 1,
                    'description' => 'Buttered Chicken'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Chicken Cordon Bleu',
                    'category_id' => $category->id,
                    'sub_category_id' => 1,
                    'description' => 'Chicken Cordon Bleu'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Country Fried Chicken',
                    'category_id' => $category->id,
                    'sub_category_id' => 1,
                    'description' => 'Country Fried Chicken'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Chicken Strips',
                    'category_id' => $category->id,
                    'sub_category_id' => 1,
                    'description' => 'Chicken Strips'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Lemon Chicken',
                    'category_id' => $category->id,
                    'sub_category_id' => 1,
                    'description' => 'Lemon Chicken'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Chicken Afritada',
                    'category_id' => $category->id,
                    'sub_category_id' => 1,
                    'description' => 'Chicken Afritada'
                ]);
                echo "OK!\n";
            } else if ($category->name == 'fish') {
                // FISH
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Fish Fillet Sweet and Sour',
                    'category_id' => $category->id,
                    'sub_category_id' => 1,
                    'description' => 'Fish Fillet Sweet and Sour'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Fish Fillet in Tausi',
                    'category_id' => $category->id,
                    'sub_category_id' => 1,
                    'description' => 'Fish Fillet in Tausi'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Fish Teriyaki',
                    'category_id' => $category->id,
                    'sub_category_id' => 1,
                    'description' => 'Fish Teriyaki'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Breaded Fish Fillet in white sauce',
                    'category_id' => $category->id,
                    'sub_category_id' => 1,
                    'description' => 'Breaded Fish Fillet in white sauce'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Shrimp Halabus',
                    'category_id' => $category->id,
                    'sub_category_id' => 1,
                    'description' => 'Shrimp Halabus'
                ]);
                echo "OK!\n";
            } else if ($category->name == 'drink') {
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Round Softdrink',
                    'category_id' => $category->id,
                    'description' => 'Round Softdrink'
                ]);
                echo "OK!\n";

                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Coffee',
                    'category_id' => $category->id,
                    'sub_category_id' => 11,
                    'description' => 'Coffee'
                ]);
                echo "OK!\n";

                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Sikwate/Kinape',
                    'category_id' => $category->id,
                    'sub_category_id' => 11,
                    'description' => 'Sikwate/Kinape'
                ]);
                echo "OK!\n";
            } else if ($category->name == 'snack') {
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Bingka',
                    'category_id' => $category->id,
                    'description' => 'Bingka'
                ]);
                echo "OK!\n";

                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Puto Maya',
                    'category_id' => $category->id,
                    'description' => 'Puto Maya'
                ]);
                echo "OK!\n";
            } else if ($category->name == 'entree') {
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Plain Rice',
                    'category_id' => $category->id,
                    'description' => 'Plain Rice'
                ]);
                echo "OK!\n";
                \App\FoodDrink::firstOrCreate([
                    'name' =>  'Lumpia Shanghai',
                    'category_id' => $category->id,
                    'description' => 'Lumpia Shanghai'
                ]);
                echo "OK!\n";
            } 
        }
        
    }
}
