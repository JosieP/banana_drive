<?php

use Illuminate\Database\Seeder;

class ProvinceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo 'Creating province: ';
        \App\Province::firstOrCreate([
            'name' =>  'bohol',
            'type' => 'province'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'alburquerque',
            'province_id' => 1,
            'price' => 856,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'alicia',
            'province_id' => 1,
            'price' => 6776,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'anda',
            'province_id' => 1,
            'price' => 8616,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'antequera',
            'province_id' => 1,
            'price' => 2024,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'baclayon',
            'province_id' => 1,
            'price' => 800,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'balilihan',
            'province_id' => 1,
            'price' => 1416,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'batuan',
            'province_id' => 1,
            'price' => 2904,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'bien unido',
            'province_id' => 1,
            'price' => 7376,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'bilar',
            'province_id' => 1,
            'price' => 2800,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'buenavista',
            'province_id' => 1,
            'price' => 5992,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'calape',
            'province_id' => 1,
            'price' => 3752,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'candijay',
            'province_id' => 1,
            'price' => 7664,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'carmen',
            'province_id' => 1,
            'price' => 3992,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'catigbian',
            'province_id' => 1,
            'price' => 2480,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'clarin',
            'province_id' => 1,
            'price' => 3856,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'corella',
            'province_id' => 1,
            'price' => 0,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'cortes',
            'province_id' => 1,
            'price' => 512,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'dagohoy',
            'province_id' => 1,
            'price' => 4888,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'danao',
            'province_id' => 1,
            'price' => 4920,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'dauis',
            'province_id' => 1,
            'price' => 10,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'dimiao',
            'province_id' => 1,
            'price' => 10,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'duero',
            'province_id' => 1,
            'price' => 10,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'garcia hernandez',
            'province_id' => 1,
            'price' => 9424,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'getafe',
            'province_id' => 1,
            'price' => 10,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'guindulman',
            'province_id' => 1,
            'price' => 10,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'inabanga',
            'province_id' => 1,
            'price' => 10,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'jagna',
            'province_id' => 1,
            'price' => 10,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'lila',
            'province_id' => 1,
            'price' => 2576,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'loay',
            'province_id' => 1,
            'price' => 1648,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'loboc',
            'province_id' => 1,
            'price' => 1504,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'loon',
            'province_id' => 1,
            'price' => 2512,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'mabini',
            'province_id' => 1,
            'price' => 7976,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'maribojoc',
            'province_id' => 1,
            'price' => 1592,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'panglao',
            'province_id' => 1,
            'price' => 2408,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'pilar',
            'province_id' => 1,
            'price' => 5600,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'presedent carlos p. garcia',
            'province_id' => 1,
            'price' => 10,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'sagbayan',
            'province_id' => 1,
            'price' => 3024,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'san isidro',
            'province_id' => 1,
            'price' => 6242,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'san miguel',
            'province_id' => 1,
            'price' => 10,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'sevilla',
            'province_id' => 1,
            'price' => 1160,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'sierra bullones',
            'province_id' => 1,
            'price' => 10,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'sikatuna',
            'province_id' => 1,
            'price' => 5432,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'tagbilaran',
            'province_id' => 1,
            'price' => 0,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'talibon',
            'province_id' => 1,
            'price' => 7120,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'trinidad',
            'province_id' => 1,
            'price' => 6688,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'tubigon',
            'province_id' => 1,
            'price' => 3568,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'ubay',
            'province_id' => 1,
            'price' => 7432,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
        \App\Province::firstOrCreate([
            'name' =>  'valencia',
            'province_id' => 1,
            'price' => 3576,
            'type' => 'municipality'
        ]);
        echo "OK!\n";
    }
}
