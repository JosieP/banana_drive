<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo 'Creating category: ';
        \App\Category::firstOrCreate([
            'name' =>  'entree',
        ]);
        echo "OK!\n";
        \App\Category::firstOrCreate([
            'name' =>  'beef',
        ]);
        echo "OK!\n";
        \App\Category::firstOrCreate([
            'name' =>  'pork',
        ]);
        echo "OK!\n";
        \App\Category::firstOrCreate([
            'name' =>  'vegetables',
        ]);
        echo "OK!\n";
        \App\Category::firstOrCreate([
            'name' =>  'dessert',
        ]);
        echo "OK!\n";
        \App\Category::firstOrCreate([
            'name' =>  'noodles or pasta',
        ]);
        echo "OK!\n";
        \App\Category::firstOrCreate([
            'name' =>  'soup',
        ]);
        echo "OK!\n";
        \App\Category::firstOrCreate([
            'name' =>  'chicken',
        ]);
        echo "OK!\n";
        \App\Category::firstOrCreate([
            'name' =>  'fish',
        ]);
        echo "OK!\n";
        \App\Category::firstOrCreate([
            'name' =>  'drink',
        ]);
        echo "OK!\n";
        \App\Category::firstOrCreate([
            'name' =>  'snack',
        ]);
        echo "OK!\n";
    }
}
