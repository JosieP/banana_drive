<?php

use Illuminate\Database\Seeder;

class MaterialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo 'Creating materials: ';
        \App\Material::firstOrCreate([
            'name' =>  'Wood Long Table with cloth',
            'unit'  =>  'each',
            'price'     =>  '350',
        ]);
        echo "OK!\n";
        \App\Material::firstOrCreate([
            'name' =>  'Plastic Long Table with cloth',
            'unit'  =>  'each',
            'price'     =>  '350',
        ]);
        echo "OK!\n";
        \App\Material::firstOrCreate([
            'name' =>  'Plastic Round Tables with cloth',
            'unit'  =>  'each',
            'price'     =>  '50',
        ]);
        echo "OK!\n";
        \App\Material::firstOrCreate([
            'name' =>  'Bride & Groom Skirting motif',
            'unit'  =>  'each',
            'price'     =>  '500',
        ]);
        echo "OK!\n";
        \App\Material::firstOrCreate([
            'name' =>  'Gift Table with Skirting',
            'unit'  =>  'each',
            'price'     =>  '300',
        ]);
        echo "OK!\n";
        \App\Material::firstOrCreate([
            'name' =>  'Buffet Table with Skirting (1 set)',
            'unit'  =>  'set',
            'price'     =>  '1000',
        ]);
        echo "OK!\n";
        \App\Material::firstOrCreate([
            'name' =>  'Cake Table with Skirting',
            'unit'  =>  'each',
            'price'     =>  '300',
        ]);
        echo "OK!\n";
        \App\Material::firstOrCreate([
            'name' =>  'Plastic Chairs with Chair Cloth',
            'unit'  =>  'each',
            'price'     =>  '50',
        ]);
        echo "OK!\n";
        \App\Material::firstOrCreate([
            'name' =>  'Letchon Table with Letchon Man',
            'unit'  =>  'each',
            'price'     =>  '500',
        ]);
        echo "OK!\n";
        // CUTLERIES
        \App\Material::firstOrCreate([
            'name' =>  'Dinner Plates (1 crate 25 pcs)',
            'unit'  =>  'crate',
            'price'     =>  '300',
        ]);
        echo "OK!\n";
        \App\Material::firstOrCreate([
            'name' =>  'Water Goblet (1 crate 25 pcs)',
            'unit'  =>  'each',
            'price'     =>  '500',
        ]);
        echo "OK!\n";
        \App\Material::firstOrCreate([
            'name' =>  '1 set Wine Glass (for the Bride & Groom)',
            'unit'  =>  'set',
            'price'     =>  '200',
        ]);
        echo "OK!\n";
        \App\Material::firstOrCreate([
            'name' =>  'Fork & Spoon Wrapped (1 tray 50 pcs)',
            'unit'  =>  'tray',
            'price'     =>  '200',
        ]);
        echo "OK!\n";
        \App\Material::firstOrCreate([
            'name' =>  'Dessert Plates (30 pcs.)',
            'unit'  =>  'crate',
            'price'     =>  '350',
        ]);
        echo "OK!\n";
        \App\Material::firstOrCreate([
            'name' =>  'Soup Bowl (50 pcs)',
            'unit'  =>  'crate',
            'price'     =>  '500',
        ]);
        echo "OK!\n";
        \App\Material::firstOrCreate([
            'name' =>  'Highball Glass (1 crate 25 pcs)',
            'unit'  =>  'crate',
            'price'     =>  '300',
        ]);
        echo "OK!\n";
        // FOOD WARMERS
        \App\Material::firstOrCreate([
            'name' =>  'Food Warmer Circle (good for 50 pax)',
            'unit'  =>  'each',
            'price'     =>  '250',
        ]);
        echo "OK!\n";
        \App\Material::firstOrCreate([
            'name' =>  'Food Warmer Oval (good for 50-75 pax)',
            'unit'  =>  'each',
            'price'     =>  '250',
        ]);
        echo "OK!\n";
        \App\Material::firstOrCreate([
            'name' =>  'Food Warmer Circle (good for 50-100 pax)',
            'unit'  =>  'each',
            'price'     =>  '250',
        ]);
        echo "OK!\n";
        \App\Material::firstOrCreate([
            'name' =>  'Food Warmer Oval Big (good for 150 pax)',
            'unit'  =>  'each',
            'price'     =>  '250',
        ]);
        echo "OK!\n";
        \App\Material::firstOrCreate([
            'name' =>  'Food Warmer Rectagular (good for 100 pax)',
            'unit'  =>  'each',
            'price'     =>  '200',
        ]);
        echo "OK!\n";
    }
}
