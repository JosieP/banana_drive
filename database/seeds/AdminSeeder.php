<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo 'Creating system admin: ';
        \App\Admin::create([
            'firstname' =>  'Rizza',
            'lastname'  =>  'Tumilap',
            'email'     =>  'admin@admin.com',
            'contact_number'     =>  '+639108999052',
            'address'     =>  'Cancatac, Corella, Bohol',
            'password'  =>  bcrypt('qweqwe'),
            'type'      =>  'admin'
        ]);
        echo "OK!\n";

        echo 'Creating user: ';
        \App\User::create([
            'firstname' =>  'User',
            'lastname'  =>  'User',
            'email'     =>  'user@user.com',
            'password'  =>  bcrypt('qweqwe'),
            'type'      =>  'customer'
        ]);
        echo "OK!\n";

        $this->call(MaterialSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(ServiceSeeder::class);
        $this->call(FoodSeeder::class);
        $this->call(ProvinceSeeder::class);
        $this->call(PackageSeeder::class);
    }
}
