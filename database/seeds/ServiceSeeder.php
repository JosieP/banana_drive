<?php

use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo 'Creating services: ';
        \App\Service::firstOrCreate([
            'name' =>  'Christianing / Bunyag',
        ]);
        echo "OK!\n";
        \App\Service::firstOrCreate([
            'name' =>  'Wedding',
        ]);
        echo "OK!\n";
        \App\Service::firstOrCreate([
            'name' =>  'Birthday',
        ]);
        echo "OK!\n";
        \App\Service::firstOrCreate([
            'name' =>  'Graduation',
        ]);
        echo "OK!\n";
        \App\Service::firstOrCreate([
            'name' =>  'Thanks Giving',
        ]);
        echo "OK!\n";
        \App\Service::firstOrCreate([
            'name' =>  'Blessing',
        ]);
        echo "OK!\n";
        \App\Service::firstOrCreate([
            'name' =>  'Death Anniversary',
        ]);
        echo "OK!\n";
        \App\Service::firstOrCreate([
            'name' =>  'Burial',
        ]);
        echo "OK!\n";
        \App\Service::firstOrCreate([
            'name' =>  'Welcome Party',
        ]);
        echo "OK!\n";
        \App\Service::firstOrCreate([
            'name' =>  'Farewell Party',
        ]);
        echo "OK!\n";
        \App\Service::firstOrCreate([
            'name' =>  'Debut',
        ]);
        echo "OK!\n";
        \App\Service::firstOrCreate([
            'name' =>  'Team Building',
        ]);
        echo "OK!\n";
    }
}
