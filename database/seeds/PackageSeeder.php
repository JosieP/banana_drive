<?php

use Illuminate\Database\Seeder;

class PackageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo 'Creating tools package: ';
        \App\Package::firstOrCreate([
            'name' =>  'Wedding Package Catering Equipment Good for 100 pax',
            'description' => '<p>Items Inclustion in the package:</p><ul><li>100 Chairs with Chair Cloth</li><li>12 Round Tables with Table Cloth &amp; Topping( 8 setters )</li><li>1 Bride-Groom table with skirting using the wedding motif</li><li>Cake table with Skirting Motif</li><li>Gift Table with Skirting Motif</li><li>Cloth Backdrop</li><li>Parents Table with Skirting Motif</li><li>Principal Sponsor Table with Skirting Motif</li><li>1 set Buffet Table with Skirting for invited guest</li><li>1 set lechon  Table with lechon man &amp; accessories</li><li>1 set Dove Cage</li><li>1 Bucket of Candies</li><li>1 Red Carpet for the Venue</li><li>1 set Wine Glasses for the Bride &amp; Groom</li><li>100 Water Goblet with Table Napkin</li><li>100 Dinner Plates</li><li>100 Fork &amp; Spoon Wrapper </li><li>6 Food Warmer</li><li>1 Water Dispenser</li><li>100 Service Plates</li><li>50 Cake Plates</li><li>50 Dessert Plates</li><li>6 Waiters/Waitress</li><li>2 Cooler of Softdrinks</li><li>1 set Cake Slicer </li><li>2 Big Tent (5x6)</li><li>2 Small Tent (3x5)</li></ul><p><br></p><p>Transportation depends on the location</p>',
            'status' => 'available',
            'type' => 'tools',
            'price' => 10000
        ]);
        echo "OK!\n";
        \App\Package::firstOrCreate([
            'name' =>  'Wedding Package Catering Equipment Good for 150 pax',
            'description' => '<p>Items Inclustion in the package:</p><ul><li>150 Chairs with Chair Cloth</li><li>18 Round Tables with Table Cloth &amp; Topping( 8 setters )</li><li>1 Bride-Groom table with skirting using the wedding motif</li><li>Cake table with Skirting Motif</li><li>Gift Table with Skirting Motif</li><li>Cloth Backdrop</li><li>Parents Table with Skirting Motif</li><li>Principal Sponsor Table with Skirting Motif</li><li>1 set Buffet Table with Skirting for invited guest</li><li>1 set lechon Table with lechon man &amp; accessories</li><li>1 set Dove Cage</li><li>1 Bucket of Candies</li><li>1 Red Carpet for the Venue</li><li>1 set Wine Glasses for the Bride &amp; Groom</li><li>150 Water Goblet with Table Napkin</li><li>150 Dinner Plates</li><li>150 Fork &amp; Spoon Wrapper</li><li>12 Food Warmer</li><li>1 Water Dispenser</li><li>100 Service Plates</li><li>50 Cake Plates</li><li>50 Dessert Plates</li><li>6 Waiters/Waitress</li><li>2 Cooler of Softdrinks</li><li>1 set Cake Slicer</li><li>3 Big Tent (5x6)</li><li>2 Small Tent (3x5)</li></ul><p><br></p><p>Transportation depends on the location</p>',
            'status' => 'available',
            'type' => 'tools',
            'price' => 15000
        ]);
        echo "OK!\n";
        \App\Package::firstOrCreate([
            'name' =>  'Wedding Package Catering Equipment Good for 200 pax',
            'description' => '<p>Items Inclustion in the package:</p><ul><li>200 Chairs with Chair Cloth</li><li>25 Round Tables with Table Cloth &amp; Topping( 8 setters )</li><li>1 Bride-Groom table with skirting using the wedding motif</li><li>Cake table with Skirting Motif</li><li>Gift Table with Skirting Motif</li><li>Cloth Backdrop</li><li>Parents Table with Skirting Motif</li><li>Principal Sponsor Table with Skirting Motif</li><li>1 set Buffet Table with Skirting for invited guest</li><li>1 set lechon Table with lechon man &amp; accessories</li><li>1 set Dove Cage</li><li>1 Bucket of Candies</li><li>1 Red Carpet for the Venue</li><li>1 set Wine Glasses for the Bride &amp; Groom</li><li>200 Water Goblet with Table Napkin</li><li>75 Highball Glass</li><li>200 Dinner Plates</li><li>200 Fork &amp; Spoon Wrapper</li><li>12 Food Warmer</li><li>1 Water Dispenser</li><li>100 Service Plates</li><li>50 Cake Plates</li><li>50 Dessert Plates</li><li>Glass Pitchers</li><li>6 Waiters/Waitress</li><li>2 Cooler of Softdrinks</li><li>1 set Cake Slicer</li><li>4 Big Tent (5x6)</li><li>2 Small Tent (3x5)</li></ul><p><br></p><p>Transportation depends on the location</p>',
            'status' => 'available',
            'type' => 'tools',
            'price' => 10000
        ]);
        echo "OK!\n";
        \App\Package::firstOrCreate([
            'name' =>  'Catering Equipment Package Good for 100 pax',
            'description' => '<p>Items Inclustion in the package:</p><ul><li>1 set Buffet Table with Skirting motif</li><li>100 Chairs with Cloth</li><li>12 Rounded Table with Cloth and Toppings</li><li>100 Dinner Plates</li><li>100 Spoon and Fork (wrapper)</li><li>100 Water Goblet</li><li>100 Table Napkin</li><li>6 Food Warmer</li><li>6 Serving Spoons</li><li>Letchon Table</li><li>Waiters/Waitress</li><li>1 Softdrinks Opener</li><li>1 Cooler of Softdrinks</li></ul><p><br></p><p>Transportation depends on the location</p>',
            'status' => 'available',
            'type' => 'tools',
            'price' => 10000
        ]);
        echo "OK!\n";
        \App\Package::firstOrCreate([
            'name' =>  'Catering Equipment Package Good for 50 pax',
            'description' => '<p>Items Inclustion in the package:</p><ul><li>1 set Buffet Table with Skirting motif</li><li>50 Chairs with Cloth</li><li>6 Rounded Table with Cloth and Toppings</li><li>50 Dinner Plates</li><li>50 Spoon and Fork (wrapper)</li><li>50 Water Goblet</li><li>50 Table Napkin</li><li>6 Food Warmer</li><li>6 Serving Spoons</li><li>1 Cooler</li></ul><p><br></p><p>Transportation depends on the location</p>',
            'status' => 'available',
            'type' => 'tools',
            'price' => 5000
        ]);
        echo "OK!\n Done!";
        \App\Package::firstOrCreate([
            'name' =>  'Menu @ 275/head',
            'status' => 'available',
            'type' => 'menu',
            'package_type' => 'seta',
            'food_max' => 5,
            'price' => 275
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Menu @ 275/head')->first()->id,
            'category_id' => 11,
            'food_id' => 50
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Menu @ 275/head')->first()->id,
            'category_id' => 11,
            'food_id' => 48
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Menu @ 275/head')->first()->id,
            'category_id' => 11,
            'food_id' => 51
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Menu @ 275/head')->first()->id,
            'category_id' => 11,
            'food_id' => 49
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Menu @ 275/head')->first()->id,
            'category_id' => 1,
            'food_id' => 1
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Menu @ 275/head')->first()->id,
            'category_id' => 1,
            'food_id' => 3
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Menu @ 275/head')->first()->id,
            'category_id' => 1,
            'food_id' => 14
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Menu @ 275/head')->first()->id,
            'category_id' => 1,
            'food_id' => 36
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Menu @ 275/head')->first()->id,
            'category_id' => 1,
            'food_id' => 27
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Menu @ 275/head')->first()->id,
            'category_id' => 10,
            'food_id' => 47
        ]);
        echo "OK!\n Done!";
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Menu @ 275/head')->first()->id,
            'category_id' => 5,
            'food_id' => 22
        ]);
        echo "OK!\n Done!";
        \App\Package::firstOrCreate([
            'name' =>  'Catering menu @ 250/head (Set A)',
            'status' => 'available',
            'type' => 'menu',
            'package_type' => 'seta',
            'food_max' => 5,
            'price' => 250
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 250/head (Set A)')->first()->id,
            'category_id' => 7,
            'food_id' => 32
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 250/head (Set A)')->first()->id,
            'category_id' => 1,
            'food_id' => 5
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 250/head (Set A)')->first()->id,
            'category_id' => 1,
            'food_id' => 37
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 250/head (Set A)')->first()->id,
            'category_id' => 1,
            'food_id' => 2
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 250/head (Set A)')->first()->id,
            'category_id' => 1,
            'food_id' => 8
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 250/head (Set A)')->first()->id,
            'category_id' => 1,
            'food_id' => 1
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 250/head (Set A)')->first()->id,
            'category_id' => 10,
            'food_id' => 47
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 250/head (Set A)')->first()->id,
            'category_id' => 5,
            'food_id' => 22
        ]);
        echo "OK!\n Done!";
        \App\Package::firstOrCreate([
            'name' =>  'Catering menu @ 250/head (Set B)',
            'status' => 'available',
            'type' => 'menu',
            'package_type' => 'setb',
            'food_max' => 5,
            'price' => 250
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 250/head (Set B)')->first()->id,
            'category_id' => 7,
            'food_id' => 34
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 250/head (Set B)')->first()->id,
            'category_id' => 1,
            'food_id' => 42
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 250/head (Set B)')->first()->id,
            'category_id' => 1,
            'food_id' => 3
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 250/head (Set B)')->first()->id,
            'category_id' => 1,
            'food_id' => 27
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 250/head (Set B)')->first()->id,
            'category_id' => 1,
            'food_id' => 16
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 250/head (Set B)')->first()->id,
            'category_id' => 1,
            'food_id' => 1
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 250/head (Set B)')->first()->id,
            'category_id' => 10,
            'food_id' => 47
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 250/head (Set B)')->first()->id,
            'category_id' => 5,
            'food_id' => 25
        ]);
        echo "OK!\n Done!";

        \App\Package::firstOrCreate([
            'name' =>  'Catering menu @ 200/head (Set A)',
            'status' => 'available',
            'type' => 'menu',
            'package_type' => 'seta',
            'food_max' => 5,
            'price' => 200
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 200/head (Set A)')->first()->id,
            'category_id' => 1,
            'food_id' => 15
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 200/head (Set A)')->first()->id,
            'category_id' => 1,
            'food_id' => 37
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 200/head (Set A)')->first()->id,
            'category_id' => 1,
            'food_id' => 2
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 200/head (Set A)')->first()->id,
            'category_id' => 1,
            'food_id' => 27
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 200/head (Set A)')->first()->id,
            'category_id' => 1,
            'food_id' => 1
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 200/head (Set A)')->first()->id,
            'category_id' => 10,
            'food_id' => 47
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 200/head (Set A)')->first()->id,
            'category_id' => 5,
            'food_id' => 23
        ]);
        echo "OK!\n Done!";
        \App\Package::firstOrCreate([
            'name' =>  'Catering menu @ 200/head (Set B)',
            'status' => 'available',
            'type' => 'menu',
            'package_type' => 'setb',
            'food_max' => 5,
            'price' => 200
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 200/head (Set B)')->first()->id,
            'category_id' => 1,
            'food_id' => 8
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 200/head (Set B)')->first()->id,
            'category_id' => 1,
            'food_id' => 42
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 200/head (Set B)')->first()->id,
            'category_id' => 1,
            'food_id' => 4
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 200/head (Set B)')->first()->id,
            'category_id' => 1,
            'food_id' => 28
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 200/head (Set B)')->first()->id,
            'category_id' => 1,
            'food_id' => 1
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 200/head (Set B)')->first()->id,
            'category_id' => 10,
            'food_id' => 47
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 200/head (Set B)')->first()->id,
            'category_id' => 5,
            'food_id' => 21
        ]);
        echo "OK!\n Done!";

        \App\Package::firstOrCreate([
            'name' =>  'Catering menu @ 350/head (Set A)',
            'status' => 'available',
            'type' => 'menu',
            'package_type' => 'seta',
            'food_max' => 6,
            'price' => 350
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 350/head (Set A)')->first()->id,
            'category_id' => 7,
            'food_id' => 34
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 350/head (Set A)')->first()->id,
            'category_id' => 1,
            'food_id' => 13
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 350/head (Set A)')->first()->id,
            'category_id' => 1,
            'food_id' => 42
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 350/head (Set A)')->first()->id,
            'category_id' => 1,
            'food_id' => 6
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 350/head (Set A)')->first()->id,
            'category_id' => 1,
            'food_id' => 36
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 350/head (Set A)')->first()->id,
            'category_id' => 1,
            'food_id' => 46
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 350/head (Set A)')->first()->id,
            'category_id' => 1,
            'food_id' => 1
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 350/head (Set A)')->first()->id,
            'category_id' => 10,
            'food_id' => 47
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 350/head (Set A)')->first()->id,
            'category_id' => 5,
            'food_id' => 22
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 350/head (Set A)')->first()->id,
            'category_id' => 5,
            'food_id' => 20
        ]);
        echo "OK!\n Done!";
        \App\Package::firstOrCreate([
            'name' =>  'Catering menu @ 350/head (Set B)',
            'status' => 'available',
            'type' => 'menu',
            'package_type' => 'setb',
            'food_max' => 6,
            'price' => 350
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 350/head (Set B)')->first()->id,
            'category_id' => 7,
            'food_id' => 35
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 350/head (Set B)')->first()->id,
            'category_id' => 1,
            'food_id' => 5
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 350/head (Set B)')->first()->id,
            'category_id' => 1,
            'food_id' => 37
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 350/head (Set B)')->first()->id,
            'category_id' => 1,
            'food_id' => 27
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 350/head (Set B)')->first()->id,
            'category_id' => 1,
            'food_id' => 44
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 350/head (Set B)')->first()->id,
            'category_id' => 1,
            'food_id' => 17
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 350/head (Set B)')->first()->id,
            'category_id' => 1,
            'food_id' => 1
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 350/head (Set B)')->first()->id,
            'category_id' => 10,
            'food_id' => 47
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 350/head (Set B)')->first()->id,
            'category_id' => 5,
            'food_id' => 25
        ]);
        \App\PackageList::firstOrCreate([
            'package_id' =>  \App\Package::where('name', 'Catering menu @ 350/head (Set B)')->first()->id,
            'category_id' => 5,
            'food_id' => 24
        ]);
        echo "OK!\n Done!";
    }
}
