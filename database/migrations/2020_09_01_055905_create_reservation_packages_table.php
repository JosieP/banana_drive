<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservationPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservation_packages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('reservation_id');
            $table->unsignedBigInteger('package_id');
            $table->integer('quantity');
            $table->time('time');
            $table->decimal('price', 18, 2)->default(0.00);
            $table->timestamps();
        });

        Schema::table('reservation_packages', function (Blueprint $table) {
            $table->foreign('reservation_id')->references('id')->on('reservations');
            $table->foreign('package_id')->references('id')->on('packages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservation_packages');
    }
}
