<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('reservation_id');
            $table->string('refference_no')->nullable();
            $table->date('payment_date')->nullable();
            $table->decimal('amount', 18, 2)->default(0.00);
            $table->string('filename')->nullable();
            $table->text('url')->nullable();
            $table->enum('type', ['unpaid', 'paid', 'refund']);
            $table->enum('status', ['waiting', 'pending', 'complete', 'cancelled']);
            $table->text('description')->nullable();
            $table->timestamps();
        });

        Schema::table('payments', function (Blueprint $table) {
            $table->foreign('reservation_id')->references('id')->on('reservations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
