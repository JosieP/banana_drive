<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('title')->nullable();
            $table->unsignedBigInteger('service_id');
            $table->unsignedBigInteger('province_id')->nullable();
            $table->unsignedBigInteger('municipality_id');
            $table->unsignedBigInteger('barangay_id');
            $table->string('address');
            $table->date('start_actual_date', 0);
            $table->string('contact_number');
            $table->decimal('amount', 18, 2)->default(0.00);
            $table->decimal('amount_paid', 18, 2)->default(0.00);
            $table->decimal('delivery_fee', 18, 2)->default(0.00);
            $table->enum('status', ['waiting', 'pending', 'approved', 'completed', 'cancelling', 'cancelled']);
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::table('reservations', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('service_id')->references('id')->on('services');
            $table->foreign('province_id')->references('id')->on('provinces');
            $table->foreign('municipality_id')->references('id')->on('provinces');
            $table->foreign('barangay_id')->references('id')->on('provinces');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
