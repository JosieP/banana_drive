<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservationPackageListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservation_package_lists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('reservation_package_id');
            $table->unsignedBigInteger('package_id')->nullable();
            $table->unsignedBigInteger('category_id')->nullable();
            $table->unsignedBigInteger('food_id')->nullable();
            $table->timestamps();
        });

        Schema::table('reservation_package_lists', function (Blueprint $table) {
            $table->foreign('reservation_package_id')->references('id')->on('reservation_packages');
            $table->foreign('package_id')->references('id')->on('packages');
            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('food_id')->references('id')->on('food_drinks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservation_package_lists');
    }
}
