require('./bootstrap');
require("vue-toastr/src/vue-toastr.scss");
require('vue2-datepicker/index.css');
require('vue-select/dist/vue-select.css');

window.Vue = require('vue');
window.moment = require("moment");

Vue.component('contact-page', require('./components/Contact.vue').default);
Vue.component('aboutus-page', require('./components/AboutUs.vue').default);
Vue.component('menu-page', require('./components/Menu.vue').default);
Vue.component('province-page', require('./components/Provinces/Province.vue').default);
Vue.component('public-dashboard-page', require('./components/Public/Dashboard.vue').default);
Vue.component('public-package-page', require('./components/Public/Package.vue').default);
Vue.component('reservation-page', require('./components/Public/Reserve.vue').default);
Vue.component('reservation-page-2', require('./components/Public/Reserve2.vue').default);
Vue.component('edit-reservation-page', require('./components/Reservations/EditReservation.vue').default);
Vue.component('edit-reservation-page-2', require('./components/Reservations/EditReservation2.vue').default);
Vue.component('reservation-page-list', require('./components/Reservations/Reservation.vue').default);
Vue.component('show-notification', require('./components/Notifications/ShowNotification.vue').default);
Vue.component('sales-report', require('./components/Reports/SalesReport.vue').default);
Vue.component('fee-management', require('./components/Reports/FeeManagement.vue').default);
Vue.component('receipt-page', require('./components/Public/Receipt.vue').default);
Vue.component('refund-page', require('./components/Refunds/Refund.vue').default);
Vue.component('home-modal', require('./components/Public/HomeModal.vue').default);
Vue.component('register-modal', require('./components/Public/Register.vue').default);
Vue.component('calendar-page', require('./components/Calendar/Calendar.vue').default);

Vue.component('user-page', require('./components/Users/User.vue').default);
Vue.component('user-details-page', require('./components/UserDetails/UserDetails.vue').default);
Vue.component('user-details', require('./components/Public/UserDetails.vue').default);
Vue.component('dashboard-page', require('./components/Dashboards/Dashboard.vue').default);
Vue.component('setting-page', require('./components/Dashboards/AccountSetting.vue').default);
Vue.component('category-page', require('./components/Categories/Category.vue').default);
Vue.component('food-page', require('./components/Foods/FoodDrinks.vue').default);
Vue.component('package-page', require('./components/Packages/Package.vue').default);
Vue.component('material-page', require('./components/Materials/Material.vue').default);
Vue.component('service-page', require('./components/Services/Service.vue').default);

Vue.component('example-component', require('./components/ExampleComponent.vue').default);


import Vue from "vue";
import Toastr from "vue-toastr";
import vSelect from 'vue-select';
import { ValidationProvider, ValidationObserver, extend } from 'vee-validate/dist/vee-validate.full';

import UserDetails from './components/Public/UserDetails';


Vue.use(Toastr);
Vue.component('v-select', vSelect)
Vue.component("vue-editor", VueEditor);
Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);
import { VueEditor } from "vue2-editor";

Vue.filter("formatDate", function(value) {
  if (value != null) {
      return moment(value).format("MMMM DD, YYYY");
  } else {
      return null;
  }
});

Vue.filter("formatDateTime", function(value) {
  if (value != null) {
      return moment(value).format("MMMM DD, YYYY hh:mm");
  } else {
      return null;
  }
});

Vue.filter("formatTime", function(value) {
  if (value != null) {
      return moment(value, 'HH:mm').format('hh:mm A');
  } else {
      return null;
  }
});

extend('unique', {
    async validate(value, obj) {
        // You might want to check if its a valid email
        // before sending to server...
        const model = obj[0];
        const id = obj[1];

        const { data } = await axios.get('/validator/unique', {
            params: {
                value: value,
                model: model,
                id: id,
            }
        });

        // server response
        if (data.valid) {
            return true;
        }

        return {
            valid: false,
            // the data object contents can be used in the message template
            data: {
                error: 'This record already exists.'//data.error
            }
        };
    },
    message: `This record already exists.` // will display the server error message.
});

extend("decimal", {
    validate: (value, { decimals = '*', separator = '.' } = {}) => {
      if (value === null || value === undefined || value === '') {
        return {
          valid: false
        };
      }
      if (Number(decimals) === 0) {
        return {
          valid: /^-?\d*$/.test(value),
        };
      }
      const regexPart = decimals === '*' ? '+' : `{1,${decimals}}`;
      const regex = new RegExp(`^[-+]?\\d*(\\${separator}\\d${regexPart})?([eE]{1}[-]?\\d+)?$`);
      return {
        valid: regex.test(value),
        data: {
          serverMessage: 'Only decimal values are available'
        }
      };
    },
    message: 'Only decimal values are available'
  })

// extend('password', {
//   params: ['target'],
//   validate(value, { target }) {
//     return value === target;
//   },
//   message: 'Password confirmation does not match'
// });

const app = new Vue({
    el: '#app',
    created() {
      this.getUser();
      this.getAdmin();
      this.getMessages();
      this.getNotifications();
      this.checkReservations();
    },
    data() {
      return {
        messages: [],
        notifications: [],
        countUnRead: null,
        countUnReadNotification: null,
        name: null,
        email: null,
        message: null,
        user: null,
        admin: null,
        details: null,
      }
    },
    methods: {
      checkReservations() {
        axios.get('/check-reservation').then(response => {
          
        });
      },
      showLoginForm()
      {
        $("#login-modal").modal('show');
      },
      showRegisterForm()
      {
        $("#registered-modal").modal('show');
      },
      showSetting()
      {
        this.$root.$emit('user:edit',  this.admin);
        $("#setting-modal").modal('show');
      },
      showMessage(message)
      {
        axios.get('/contact-us/' + message.id).then(response => {
          this.name = response.data.message.name;
          this.email = response.data.message.email;
          this.message = response.data.message.message;
          this.getMessages();
        }).catch(e => {
          console.log(e);
        });
        $('#message-modal').modal('show');
      },
      getUser()
      {
          axios.get('/get-user').then(response => {
              this.user = response.data.user;
              this.details = response.data.details;
          }).catch(e => {
              console.log(e);
          });
      },
      getAdmin()
      {
          axios.get('/get-admin').then(response => {
              this.admin = response.data.admin;
          }).catch(e => {
              console.log(e);
          });
      },
      showReservationForm()
      {
          if (this.details == null) {
              $('#user-details-modal').modal('show');
          } else {
              window.location = '/reservation/create';
          }
      },
      getMessages()
      {
        axios.get('/contact-us').then(response => {
          this.messages = response.data.messages;
          this.countUnRead = response.data.countUnRead;
        });
      },

      getNotifications()
      {
        axios.get('/notifications').then(response => {
          this.notifications = response.data.notifications;
          this.countUnReadNotification = response.data.countUnRead;
        });
      }
    },
});
