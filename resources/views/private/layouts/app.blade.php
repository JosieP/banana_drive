<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>@yield('title') Banana Drive</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="/assets/fontawesome-free/css/all.min.css">
  
  <link rel="stylesheet" href="/assets/css/ionicons.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="/assets/css/OverlayScrollbars.min.css">

  <!-- Theme style -->
  <link rel="stylesheet" href="/assets/css/adminlte.min.css">
  @yield('styles')
  <!-- Google Font: Source Sans Pro -->
  <link href="/assets/css/google-font.css" rel="stylesheet">
  <style lang="stylesheet">
    body {
      font-family: 'Nunito', sans-serif !important;
      font-size: 14px !important;
    }
    .unread {
      background-color: #b4dbfd !important;
    }
  </style>

</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
  <div id="app">
    <div class="wrapper">
        
      @include('private.layouts.navbar')
      @include('private.layouts.sidebar')

      @yield('content')
    </div>
  </div>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}"></script>
    <!-- REQUIRED SCRIPTS -->
    <!-- jQuery -->
    <!-- <script src="/assets/js/jquery.min.js"></script> -->
    <!-- overlayScrollbars -->
    <script src="/assets/js/jquery.overlayScrollbars.min.js"></script>
    <!-- AdminLTE App -->
    <script src="/assets/js/adminlte.min.js"></script>

    <!-- OPTIONAL SCRIPTS -->
    <!-- <script src="/assets/js/demo.js"></script> -->

    <!-- PAGE PLUGINS -->
    <!-- jQuery Mapael -->
    <script src="/assets/js/jquery.mousewheel.js"></script>
    <script src="/assets/js/raphael.min.js"></script>
    <script src="/assets/js/jquery.mapael.min.js"></script>
    <script src="/assets/js/usa_states.min.js"></script>
    <!-- ChartJS -->
    <script src="/assets/js/Chart.min.js"></script>
    @yield('scripts')
</body>
</html>