<!-- Modal -->
<div class="modal fade" id="message-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered form-dark" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2>Message</h2>
                  <label for="name">Name : </label> @{{ name }} <br>
                  <label for="name">Email : </label> @{{ email }} <br>
                  <label for="name">Message : </label> @{{ message }} <br>
                <div class="form-group">
                  <div class="col-md-12 text-right">
                    <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal" aria-label="Close"> Close </button>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end Modal -->
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-comments"></i>
          <span v-show="countUnRead != 0" class="badge badge-danger navbar-badge">@{{ countUnRead }}</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" style="max-height: 550px; overflow: auto;">
          <div class="text-center p-2" v-show="messages.length == 0" style="font-size: 11px;">- NO MESSAGE -</div>
          <div v-show="messages.length != 0" v-for="(message, index) in messages">
            <a href="#" @click.prevent="showMessage(message)" class="dropdown-item" :class="{unread : !message.isread }">
              <!-- Message Start -->
              <div class="media">
                <!-- <img src="/assets/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle"> -->
                <div class="media-body">
                  <h3 class="dropdown-item-title">
                    <b>@{{ message.name }}</b>
                  </h3>
                  <p class="text-sm">@{{ message.message }}</p>
                  <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> @{{ message.time }}</p>
                </div>
              </div>
              <!-- Message End -->
            </a>
            <div class="dropdown-divider"></div>
          </div>
          <!-- <a href="#" class="dropdown-item dropdown-footer">See All Messages</a> -->
        </div>
      </li>
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-bell"></i>
          <span v-show="countUnReadNotification != 0" class="badge badge-warning navbar-badge">@{{ countUnReadNotification }}</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" style="max-height: 550px; overflow: auto;">
          <div class="text-center p-2" v-show="notifications.length == 0" style="font-size: 11px;">- NO NOTIFICATION -</div>
          <div v-show="notifications.length != 0" v-for="(message, i) in notifications">
            <a :href="'/notifications/'+ message.id + '/edit'" class="dropdown-item" :class="{unread : !message.isread }">
              <!-- Message Start -->
              <div class="media">
                <!-- <img src="/assets/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle"> -->
                <div class="media-body">
                  <h3 class="dropdown-item-title">
                    <b>@{{ message.sender }}</b>
                  </h3>
                  <p class="text-sm" v-html="message.body"></p>
                  <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> @{{ message.time }}</p>
                </div>
              </div>
              <!-- Message End -->
            </a>
            <div class="dropdown-divider"></div>
          </div>
          <!-- <a href="#" class="dropdown-item dropdown-footer">See All Messages</a> -->
        </div>
      </li>
      @if(Auth::user() != null)
      <li class="nav-item">
        <a class="nav-link" data-toggle="dropdown" href="#"> Hi {{ Auth::user()->firstname }} !</a>
        <div class="dropdown-menu dropdown-menu dropdown-menu-right">
          <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="dropdown-item">
          <i class="fas fa-sign-out-alt mr-2" ></i> Logout
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item" @click.prevent="showSetting()">
          <i class="fas fa-user-cog mr-2"></i> Settings
          </a>
          <!-- <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-file mr-2"></i> 3 new reports
          </a> -->
        </div>
      </li>
      @endif
      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
      </form>
      <!-- <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i
            class="fas fa-th-large"></i></a>
      </li> -->
    </ul>
  </nav>
  <setting-page></setting-page>
  <!-- /.navbar -->