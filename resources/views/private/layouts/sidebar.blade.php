<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/home" class="brand-link">
      <img src="/assets/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Banana Drive</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{ url('/admin/home') }}" class="nav-link {{ Request::path() == 'admin/home' ? 'active' : '' }}" title="Dashboard">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('/admin/users') }}" class="nav-link {{ Request::path() == 'admin/users' ? 'active' : '' }}" title="Users">
              <i class="nav-icon fas fa-users-cog"></i>
              <p>
                Admins & Users
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('/admin/user-details') }}" class="nav-link {{ Request::path() == 'admin/user-details' ? 'active' : '' }}" title="User Details">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Customer Details
              </p>
            </a>
          </li>
          <li class="nav-item ">
            <a href="{{ url('/admin/food-drinks') }}" class="nav-link {{ Request::path() == 'admin/food-drinks' ? 'active' : '' }}" title="Foods & Drinks">
              <i class="nav-icon fas fa-hamburger"></i>
              <p>
                Foods & Drinks
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('/admin/packages') }}" class="nav-link {{ Request::path() == 'admin/packages' ? 'active' : '' }}" title="Packages">
              <i class="nav-icon fas fa-dice-d6"></i>
              <p>
                Packages
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('/admin/reservations') }}" class="nav-link {{ Request::path() == 'admin/reservations' ? 'active' : '' }}">
              <i class="nav-icon fas fa-calendar-alt"></i>
              <p>
                Reservations
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('/admin/refunds') }}" class="nav-link {{ Request::path() == 'admin/refunds' ? 'active' : '' }}">
              <i class="nav-icon fas fa-funnel-dollar"></i>
              <p>
                Refunds
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('/admin/calendar') }}" class="nav-link {{ Request::path() == 'admin/calendar' ? 'active' : '' }}">
              <i class="nav-icon far fa-calendar-alt"></i>
              <p>
                Calendar Event
              </p>
            </a>
          </li>
          <li class="nav-header">Setup</li>
          <li class="nav-item">
            <a href="{{ url('/admin/categories') }}" class="nav-link {{ Request::path() == 'admin/categories' ? 'active' : '' }}" title="Category">
              <i class="nav-icon fas fa-scroll"></i>
              <p>
                Category
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('/admin/materials') }}" class="nav-link {{ Request::path() == 'admin/materials' ? 'active' : '' }}" title="Materials">
              <i class="nav-icon fas fa-tools"></i>
              <p>
                Materials 
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('/admin/services') }}" class="nav-link {{ Request::path() == 'admin/services' ? 'active' : '' }}" title="Services & Events">
              <i class="nav-icon fas fa-list-ul"></i>
              <p>
                Services & Events
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('/admin/provinces') }}" class="nav-link {{ Request::path() == 'admin/provinces' ? 'active' : '' }}" title="Provinces & Municipality">
              <i class="nav-icon fas fa-building"></i>
              <p>
                Provinces & Municipality
              </p>
            </a>
          </li>
          <li class="nav-header">Reports</li>
          <li class="nav-item">
            <a href="/admin/sales-report" class="nav-link {{ Request::path() == 'admin/sales-report' ? 'active' : '' }}" title="Sales Report">
              <i class="nav-icon fas fa-scroll"></i>
              <p>
                Sales Report
              </p>
            </a>
          </li>
          <!-- <li class="nav-item">
            <a href="/admin/fee-management" class="nav-link {{ Request::path() == 'admin/fee-management' ? 'active' : '' }}" title="Fee Management">
              <i class="nav-icon far fa-folder-open"></i>
              <p>
                Fee Management
              </p>
            </a>
          </li> -->
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
  