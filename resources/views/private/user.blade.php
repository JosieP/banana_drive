@extends('private.layouts.app')

@section('title')
Users | 
@endsection()

@section('styles')
    <link rel="stylesheet" href="/assets/css/dataTables.bootstrap4.min.css">
@endsection()

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <user-page date="{{ Request::input('date') }}"></user-page>
  </div>
  <!-- /.content-wrapper -->
@endsection()

@section('scripts')
    <script src="/assets/js/jquery.dataTables.min.js"></script>
    <script src="/assets/js/dataTables.bootstrap4.min.js"></script>
@endsection()