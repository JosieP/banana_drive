@extends('private.layouts.app')

@section('title')
Categories | 
@endsection()

@section('styles')
    <link rel="stylesheet" href="/assets/css/dataTables.bootstrap4.min.css">
@endsection()

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <calendar-page></calendar-page>
  </div>
  <!-- /.content-wrapper -->
@endsection()

@section('scripts')
    <script src="/assets/js/jquery.dataTables.min.js"></script>
    <script src="/assets/js/dataTables.bootstrap4.min.js"></script>
@endsection()