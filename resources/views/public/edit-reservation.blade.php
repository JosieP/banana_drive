@extends('public.layouts.app')

@section('styles')
@endsection

@section('title')
Edit Reservation
@endsection

@section('content')
    <edit-reservation-page-2 id="{{ Request::segment(2) }}"></edit-reservation-page-2>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.4.0/bootbox.min.js"></script>
@endsection()