@extends('public.layouts.app')

@section('styles')
@endsection

@section('title')
Reservation
@endsection

@section('content')
    <reservation-page-2></reservation-page-2>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.4.0/bootbox.min.js"></script>
@endsection()