@extends('public.layouts.app')

@section('styles')
@endsection

@section('title')
Home
@endsection

@section('content')

    <section class="site-cover" style="background-image: url(images/bg_3.jpg);" id="section-home">
        <div class="container">
        <!-- style="height: 65vh;" -->
            <div class="row align-items-center justify-content-center text-center site-vh-50">
                <div class="col-md-12">
                    <h1 class="site-heading site-animate mb-3">Welcome To Banana Drive</h1>
                    <h2 class="h5 site-subheading mb-5 site-animate">Come and eat well with our delicious &amp; healthy foods.</h2> 
                    @if(Auth::check())   
                        <p><a href="#" target="_blank" @click.prevent="showReservationForm()" class="btn btn-outline-white btn-lg site-animate" style="background-color: #fff;">Reserve Now</a></p>
                    @else
                        <p><a href="#" target="_blank" @click.prevent="showLoginForm()" class="btn btn-outline-white btn-lg site-animate" style="background-color: #fff;">Reserve Now</a></p>
                    @endif
                </div>
            </div>
        </div>
    </section>
    <!-- END section -->    
    <user-details></user-details>
    <public-package-page></public-package-page>

    <menu-page></menu-page>
    <home-modal></home-modal>

@endsection

@section('scripts')
@endsection()
