<!-- Modal -->
<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered form-dark" role="document">
        <div class="modal-content">
            
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2>Login</h2>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
                            <div class="col-md-12 w-75" style="margin: 0 auto;">
                                <label for="email">Email Address/Username</label>
                                <input type="text" id="email" class="form-control rounded-pill btn-shadow @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 w-75" style="margin: 0 auto;">
                                <label for="password">Password</label>
                                <input type="password" id="password" class="form-control rounded-pill btn-shadow @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 w-75" style="margin: 0 auto;">
                                <input style="display: none;" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} id="remember" class="checkbox">&nbsp;
                                <label for="remember" style="padding-left: 18px;">
                                    Remember Me
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-sm btn-primary w-75 rounded-pill btn-shadow">Log in</button>
                            </div>
                        </div>
                    </form>
                <div class="form-group">
                    <div class="col-md-12 text-right">
                        <p>Not a member? <a href="#" @click.prevent="showRegisterForm()">Sign Up</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>