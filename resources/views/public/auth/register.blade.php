<!-- Modal -->
<div class="modal fade" id="register-modals" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2>Register</h2>
                <form method="POST" action="{{ route('register') }}">
                @csrf
                <div class="row">
                    <div class="col-md-6" style="margin: 0 auto;">
                        <div class="form-group">
                            <!-- <label for="firstname">Firstname</label> -->
                            <input type="text" id="firstname" placeholder="Enter Firstname" class="form-control rounded-pill btn-shadow @error('firstname') is-invalid @enderror" name="firstname" value="{{ old('firstname') }}" required autocomplete="firstname" autofocus>
                            @error('firstname')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6" style="margin: 0 auto;">
                        <div class="form-group">
                            <!-- <label for="lastname">Lastname</label> -->
                            <input type="text" id="lastname" placeholder="Enter Lastname" class="form-control rounded-pill btn-shadow @error('lastname') is-invalid @enderror" name="lastname" value="{{ old('lastname') }}" required autocomplete="lastname">
                            @error('lastname')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 w-75" style="margin: 0 auto;">
                        <div class="form-group">
                            <!-- <label for="email">Email</label> -->
                            <input type="text" id="email" placeholder="Enter Email/Username" class="form-control rounded-pill btn-shadow @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6 w-75" style="margin: 0 auto;">
                        <div class="form-group">
                            <!-- <label for="password">Password</label> -->
                            <select name="gender" id="gender" class="form-control rounded-pill btn-shadow @error('gender') is-invalid @enderror" value="{{ old('gender') }}" required autocomplete="gender">
                                <option value="male">Male</option>
                                <option value="female">Female</option>
                            </select>
                            @error('gender')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 w-75" style="margin: 0 auto;">
                        <div class="form-group">
                            <!-- <label for="password">Password</label> -->
                            <input type="password" id="password" placeholder="Enter Password" class="form-control rounded-pill btn-shadow @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" required autocomplete="new-password">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6 w-75" style="margin: 0 auto;">
                        <div class="form-group">
                            <!-- <label for="password-confirm">Confirm Password</label> -->
                            <input id="password-confirm" type="password" placeholder="Enter Confirm Password" class="form-control rounded-pill btn-shadow" name="password_confirmation" required autocomplete="new-password">
                        </div>
                    </div>
                </div>
                    <div class="form-group">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-sm btn-primary w-75 rounded-pill btn-shadow">Sign up</button>
                        </div>
                    </div>
                </form>
                <div class="form-group">
                    <div class="col-md-12 text-right">
                        <p>Already a member? <a href="#">Log in</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>