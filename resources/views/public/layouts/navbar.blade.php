<nav class="navbar navbar-expand-lg navbar-dark site_navbar bg-dark site-navbar-light" id="site-navbar">
    <div class="container">
        <a class="navbar-brand" href="/home"><img src="/assets/img/logo.png" alt="" width="75px"></a> 
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#site-nav" aria-controls="site-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span> Menu
        </button>

        <div class="collapse navbar-collapse" id="site-nav">
            <ul class="navbar-nav ml-auto">
            <li class="nav-item {{ Request::path() == '/' ? 'active' : '' }}"><a href="{{ url('/') }}" class="nav-link">Home</a></li>
            <li class="nav-item {{ Request::path() == 'about-us' ? 'active' : '' }}"><a href="{{ url('/about-us') }}" class="nav-link">About</a></li>
            <li class="nav-item {{ Request::path() == 'menu' ? 'active' : '' }}"><a href="{{ url('/menu') }}" class="nav-link">Menu</a></li>
            <li class="nav-item {{ Request::path() == 'contact' ? 'active' : '' }}"><a href="{{ url('/contact') }}" class="nav-link">Contact</a></li>
            @if(Auth::check())
                <li class="nav-item {{ Request::path() == 'dashboard' ? 'active' : '' }}"><a href="{{ url('/dashboard') }}" class="nav-link">My Dashboard</a></li>
            @else
                <li class="nav-item"><a href="#" class="nav-link"><span @click.prevent="showLoginForm()">Login</span> / <span @click.prevent="showRegisterForm()">Register</span></a></li>
            @endif
            </ul>
        </div>
    </div>
</nav>
<register-modal></register-modal>
