<footer class="site-footer site-bg-dark site-section">
    <div class="container">
        <div class="row mb-5">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4 site-animate">
                        <h2 class="site-heading-2"><a href="{{ url('/about-us') }}" class="py-2 d-block">About Us</a></h2>
                        <p>Banana Drive Catering Services is one of the best caterer in Bohol that offers different types of catering packages for all occasions and events.</p>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3 site-animate">
                        <div class="site-footer-widget mb-4">
                            <h2 class="site-heading-2">Banana Drive</h2>
                            <ul class="list-unstyled">
                                <li><a href="{{ url('/home') }}" class="py-2 d-block">Home</a></li>
                                <li><a href="{{ url('/menu') }}" class="py-2 d-block">Menus</a></li>
                                <li><a href="{{ url('/contact-us') }}" class="py-2 d-block">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                    @if(Auth::check())
                    <!-- <div class="col-md-4 site-animate">
                        <div class="site-footer-widget mb-4">
                            <h2 class="site-heading-2">Reservation</h2>
                            <p><a href="https://colorlib.com/" target="_blank" class="btn btn-outline-white btn-lg site-animate" data-toggle="modal" data-target="#reservationModal">Reserve Now</a></p>
                        </div>
                    </div> -->
                    @else
                    <div class="col-md-4 site-animate">
                        <div class="site-footer-widget mb-4">
                            <h2 class="site-heading-2">Login</h2>
                            
                                <form method="POST" action="{{ route('login') }}">
                                    @csrf

                                    <div class="form-group row">
                                        <label for="email" style="color: white;">{{ __('E-Mail Address') }}</label>
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" style="height: 40px !important;" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <div class="form-group row">
                                        <label for="password" style="color: white;">{{ __('Password') }}</label>
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" style="height: 40px !important;" name="password" required autocomplete="current-password">

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <div class="form-group row">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                            <label class="form-check-label" for="remember" style="color: white;">
                                                {{ __('Remember Me') }}
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <button type="submit" class="btn btn-block btn-primary">
                                            {{ __('Login') }}
                                        </button>
                                    </div>
                                </form>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</footer>