<!DOCTYPE html>
<html lang="en">
    <head>
        <title>@yield('title') - Banana Drive</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        
        <link rel="stylesheet" href="/assets/css/googleapis.css">
        <link rel="stylesheet" href="/assets/css/open-iconic-bootstrap.min.css">
        <link rel="stylesheet" href="/assets/css/animate.css">
        
        <link rel="stylesheet" href="/assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="/assets/css/owl.theme.default.min.css">
        <link rel="stylesheet" href="/assets/css/magnific-popup.css">
        <link rel="stylesheet" href="/assets/fontawesome-free/css/all.min.css">

        <link rel="stylesheet" href="/assets/css/bootstrap-datepicker.css">
        <link rel="stylesheet" href="/assets/css/jquery.timepicker.css">

        <link rel="stylesheet" href="/assets/css/icomoon.css">
        @yield('styles')
        <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
    </head>

    <body data-spy="scroll" data-target="#site-navbar" data-offset="200">
        <div id="app">
            @include('public.auth.login')
            @include('public.auth.register')
            @include('public.layouts.navbar')
            @yield('content')
            @include('public.layouts.footer')
        </div>

        <script src="{{ mix('/js/app.js') }}"></script>
        <script src="/assets/js/popper.min.js"></script>
        <script src="/assets/js/jquery.easing.1.3.js"></script>
        <script src="/assets/js/jquery.waypoints.min.js"></script>
        <script src="/assets/js/owl.carousel.min.js"></script>
        <script src="/assets/js/jquery.magnific-popup.min.js"></script>

        <script src="/assets/js/bootstrap-datepicker.js"></script>
        <script src="/assets/js/jquery.timepicker.min.js"></script>
        
        <script src="/assets/js/jquery.animateNumber.min.js"></script>
        

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
        <!-- <script src="/assets/js/google-map.js"></script> -->

        <script src="/assets/js/main.js"></script>
        @yield('scripts')

    </body>