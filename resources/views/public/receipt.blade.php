<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Invoice</title>
    <link rel="stylesheet" href="/assets/css/adminlte.min.css">
    <!-- <link href="/assets/css/google-font.css" rel="stylesheet"> -->
    <style lang="stylesheet">
    .row:after,.row:before{
            display:table;
            content:" ";
        }
        .row:after{clear:both}
        .col-md-1{width:8.33333333%}
        .col-md-2{width:16.66666667%}
        .col-md-3{width:25%}
        .col-md-4{width:33.33333333%}
        .col-md-5{width:41.66666667%}
        .col-md-6{width:50%}
        .col-md-7{width:58.33333333%}
        .col-md-8{width:66.66666667%}
        .col-md-9{width:75%}
        .col-md-10{width:83.33333333%}
        .col-md-11{width:91.66666667%}
        .col-md-12{width:100%}
        .col-md,.col-md-1,.col-md-10,.col-md-11,.col-md-12,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9,.col-md-auto{
            float:left;
            position:relative;
            min-height:1px;
            padding-right:15px;
            padding-left:15px;
        }
        .header {
            background-color: #478ed2;
            font-size: 12px;
            color: #fff;
        }
        label {
            color: #06060673;
            font-size: 12px;
        }
        hr {
            border-top: 5px solid #478ed2 !important;
            border-radius: 50px;
        }
        .table-content {
            font-size: 12px;
        }
        .total {
            font-size: 16px;
        }
        th,
        .label-total {
            color: #478ed2;
        }
        .btn-disabled { text-decoration: none; }
        .btn-disabled:hover { cursor: default; background-color: transparent !important; }
        .container {
            width: 100%;
            padding-right: 7.5px;
            padding-left: 7.5px;
            margin-right: auto;
            margin-left: auto;
        }
        @media print {
        /* style sheet for print goes here */
            .noprint {
                visibility: hidden;
            }
            .div-price {
                font-size: 40px !important;
            }
        }
    </style>
</head>
<body>
    <div class="container" style="margin-top: .5rem!important;">
        <div class="header">
            <div class="row" style="padding: 1.5rem!important;">
                <div class="col-md-4" style="font-size: 50px;">
                    <span>INVOICE</span>
                </div>
                <div class="col-md-4 text-right">
                    <div>0961 230 7637</div>
                    <div>bananadrive_catering@yahoo.com</div>
                    <div>bananadrive_catering@yahoo.com</div>
                </div>
                <div class="col-md-4 text-right">
                    <div>
                    M Lumain St, <br> Corella, Bohol, <br> 6300 Philippines
                    </div>
                </div>
            </div>
        </div>
        @if(isset($b))
            <div class="content">
                <div class="row p-4">
                    <div class="col-md-4">
                        <label>Billed To</label>
                        <div><b>{{ $b->user->firstname }} {{ $b->user->lastname }}</b></div>
                        <div><b>{{ $b->user_details->address }}</b></div>
                    </div>
                    <div class="col-md-4">
                        <label>Invoice Number</label>
                        <div>
                            <b>{{ str_pad($b->id, 8, 0, STR_PAD_LEFT) }}</b>
                        </div>
                        <label>Date of Issue</label>
                        <div>
                            <b>{{ date('F d, Y', strtotime($b->created_at)) }}</b>
                        </div>
                    </div>
                    <div class="col-md-4 text-right">
                        <label>Invoice Total</label>
                        <div style="font-size: 50px;" class="div-price">Php {{ number_format($b->amount, 2) }}</div>
                        @if($b->status == 'pending')
                            <button class="btn btn-disabled btn-sm btn-block btn-outline-danger" style="color: rgb(184, 27, 42);">{{ $b->status }} - {{ $b->type }}</button>
                        @else
                            <button class="btn btn-disabled btn-sm btn-block btn-outline-info" style="color: rgb(18, 168, 191);">{{ $b->status }} - {{ $b->type }}</button>
                        @endif
                    </div>
                </div>
            </div>
            <hr>
            <div class="table-content">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th class="text-right">Unit Cost</th>
                            <th class="text-right">Quantity</th>
                            <th class="text-right">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($b->reservation_lists as $list)
                            <tr>
                                <td>{{ $list['name'] }}</td>
                                <td class="text-right">Php {{ number_format($list['price'], 2) }}</td>
                                <td class="text-right">{{ $list['quantity'] }}</td>
                                <td class="text-right">Php {{ number_format($list['price'] * $list['quantity'], 2) }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <hr>
            <div class="row total mt-4">
                <div class="col-md-8">
                </div>
                <div class="col-md-2 text-right label-total">
                        <div><b>Paid Amount</b></div>
                        <div><b>Total Amount</b></div>
                </div>
                <div class="col-md-2 text-right">
                        <div>Php {{ number_format($b->reservation->amount_paid, 2) }}</div>
                        <div>Php {{ number_format($b->reservation->amount, 2) }}</div>
                </div>
            </div>
            <div class="row total mt-3">
                <div class="col-md-8">
                    <!-- <label>Invoice Term</label>
                    <div><b>fasfsdf</b></div> -->
                </div>
                <div class="col-md-2 text-right label-total">
                        <div><b>Remaining Balance</b></div>
                </div>
                <div class="col-md-2 text-right">
                        <div>Php {{ number_format(($b->reservation->amount - $b->reservation->amount_paid), 2) }}</div>
                </div>
            </div>
            @if($b->status == 'complete')
                <div class="row mt-5 mb-5">
                    <div class="col-md-12 text-right no-print">
                        <button onclick="window.print()" class="btn btn-sm btn-primary">Print</button>
                        <a href="{{ url('/show-receipt-pdf/'.$b->id) }}" target="_blank" class="btn btn-sm btn-outline-primary">Download PDF</a>
                    </div>
                </div>
            @endif
        @else 
            <div class="content">
                <div class="row p-4">
                    <div class="col-md-4">
                        <label>Billed To</label>
                        <div><b>{{ $user->firstname }} {{ $user->lastname }}</b></div>
                        <div><b>{{ $user_details->address }}</b></div>
                    </div>
                    <div class="col-md-4">
                        <label>Invoice Number</label>
                        <div>
                            <b>{{ str_pad($id, 8, 0, STR_PAD_LEFT) }}</b>
                        </div>
                        <label>Date of Issue</label>
                        <div>
                            <b>{{ date('F d, Y', strtotime($created_at)) }}</b>
                        </div>
                    </div>
                    <div class="col-md-4 text-right">
                        <label>Invoice Total</label>
                        <div style="font-size: 50px;" class="div-price">Php {{ number_format($amount, 2) }}</div>
                        @if($status == 'pending')
                            <button class="btn btn-disabled btn-sm btn-block btn-outline-danger" style="color: rgb(184, 27, 42);">{{ $status }} - {{ $type }}</button>
                        @else
                            <button class="btn btn-disabled btn-sm btn-block btn-outline-info" style="color: rgb(18, 168, 191);">{{ $status }} - {{ $type }}</button>
                        @endif
                    </div>
                </div>
            </div>
            <hr>
            <div class="table-content">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th class="text-right">Unit Cost</th>
                            <th class="text-right">Quantity</th>
                            <th class="text-right">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($reservation_lists as $list)
                            <tr>
                                <td>{{ $list['name'] }}</td>
                                <td class="text-right">Php {{ number_format($list['price'], 2) }}</td>
                                <td class="text-right">{{ $list['quantity'] }}</td>
                                <td class="text-right">Php {{ number_format($list['price'] * $list['quantity'], 2) }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="row total mt-4">
                <div class="col-md-8">
                </div>
                <div class="col-md-2 text-right label-total">
                        <div><b>Paid Amount</b></div>
                        <div><b>Total Amount</b></div>
                </div>
                <div class="col-md-2 text-right">
                        <div>Php {{ number_format($reservation->amount_paid, 2) }}</div>
                        <div>Php {{ number_format($reservation->amount, 2) }}</div>
                </div>
            </div>
            <div class="row total mt-3">
                <div class="col-md-8">
                </div>
                <div class="col-md-2 text-right label-total">
                        <div><b>Remaining Balance</b></div>
                </div>
                <div class="col-md-2 text-right">
                        <div>Php {{ number_format(($reservation->amount - $reservation->amount_paid), 2) }}</div>
                </div>
            </div>
            @if($status == 'complete')
                <div class="row mt-5">
                    <div class="col-md-12 text-right no-print">
                        <button onclick="window.print()" class="btn btn-sm btn-primary">Print</button>
                        <!-- <a href="{{ url('/show-receipt-pdf/'.$id) }}" target="_blank" class="btn btn-sm btn-outline-primary">Download PDF</a> -->
                    </div>
                </div>
            @endif
        @endif
    </div>
    <script src="/assets/js/adminlte.min.js"></script>
</body>
</html>