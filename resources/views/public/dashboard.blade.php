@extends('public.layouts.app')

@section('styles')
    <link rel="stylesheet" href="/assets/css/dataTables.bootstrap4.min.css">
@endsection

@section('title')
My Dashboard
@endsection

@section('content')
    <public-dashboard-page></public-dashboard-page>
@endsection

@section('scripts')
    <script src="/assets/js/jquery.dataTables.min.js"></script>
    <script src="/assets/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.4.0/bootbox.min.js"></script>
@endsection()
