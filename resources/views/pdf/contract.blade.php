<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>CONTRACT OF AGREEMENT</title>
    <style lang="stylesheet">
        @page { margin: 96px; }
        body {
            /* font-family: "Times New Roman", Times, serif; */
            font-size: 14px;
        }
        .text-center,
        .text-address,
        .text-contact, 
        .text-title {
            text-align: center;
        }
        .row{
            margin-right:-15px;
            margin-left:-15px;
        }
        .row:after,.row:before{
            display:table;
            content:" ";
        }
        .row:after{clear:both}
        .col-md-1{width:8.33333333%}
        .col-md-4{
            width:33.33333333%;
        }
        .col-md-3{width:25%}.col-md-2{width:16.66666667%}
        .col-md-4{width:33.33333333%}
        .col-md-5{width:41.66666667%}
        .col-md-6{width:50%}
        .col-md-7{width:58.33333333%}
        .col-md-8{width:66.66666667%}
        .col-md-9{width:75%}
        .col-md-10{width:83.33333333%}
        .col-md-11{width:91.66666667%}
        .col-md-12{width:100%}
        .col-md,.col-md-1,.col-md-10,.col-md-11,.col-md-12,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9,.col-md-auto{
            float:left;
            position:relative;
            min-height:1px;
            padding-right:15px;
            padding-left:15px;
        }
    </style>
</head>
<body>  
    <div style="position: absolute;">
        <img src="http://bananadrive.tk/assets/img/logo.png" alt="" width="150px">
    </div>
    <div class="text-center" style="font-family: Apple Chancery, cursive !important; font-style: oblique; font-size: 26px; padding-bottom: 2px; text-decoration: underline;">Banana Drive Catering Services</div>
    <div class="text-address">
        Mezzanine Floor, Rose Bldg. <br> CPG Avenue, Tagbilaran City, Bohol
    </div>
    <div class="text-contact">
        Contact us: <br>
        Globe Cell No. : 09276936024 or 09056342560<br>
        Smart Cell No. : 09612307637 or 09476239923<br>
        Telephone No. : 501-7021<br>
        Email us at: <a href = "mailto: bananadrive_catering@yahoo.com">bananadrive_catering@yahoo.com</a>
    </div>
    <br>
    <div class="text-title" style="font-size: 20px; padding-bottom: 5px;">
        <b>CONTRACT OF AGREEMENT</b>
    </div>
    <div class="text-content">
        <div>Name of Contact Person: <span>{{ $name }}</span></div>
        <div>Address: <span>{{ $address }}</span></div>
        <div class="row">
            <div class="col-md-6">
                Contact Number: <span>{{ $contact }}</span>
            </div>
            <div class="col-md-6">
                Date Reserved: <span>{{ $date_reserve }}</span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                Nature of Function: <span>{{ $nature }}</span>
            </div>
            <div class="col-md-6">
                Date of Function: <span>{{ $date_function }}</span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                Venue: <span>{{ $venue }}</span>
            </div>
            <div class="col-md-4">
                Motif: <span>{{ $motif }}</span>
            </div>
            <div class="col-md-4">
                Time: <span>{{ $time }}</span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                Rate per Person: <span>{{ $rate_person }}</span>
            </div>
            <div class="col-md-4">
                Guaranteed no of Person: <span>{{ $noperson }}</span>
            </div>
            <div class="col-md-4">
                Reserve: <span>{{ $reserve }}</span>
            </div>
        </div>
        <div class="row" style="margin-bottom: 5px;">
            <div class="col-md-6" style="border-top: 1px solid #000;height: 350px;">
                @foreach($reservation_packages as $p)
                    <div>
                        {{ $p['quantity'] }} x {{ $p['packages']['name'] }}     -    <b>Php {{ number_format(($p['price'] * $p['quantity']), 2) }}</b>
                    </div>
                @endforeach
                <hr>
                <div>
                    Total Amount : <b>Php {{ number_format($reservation['amount'], 2) }}</b>
                </div>
                <div>
                    Paid Amount : <b>Php {{ number_format($reservation['amount_paid'], 2) }}</b>
                </div>
                <div>
                    Remaining Balance : <b>Php {{ number_format(($reservation['amount'] - $reservation['amount_paid']), 2) }}</b>
                </div>
            </div>
            <div class="col-md-5" style="border-top: 1px solid #000; border-left: 1px solid #000;">
                @foreach($reservation_packages as $p)
                    <div>
                        {{ $p['packages']['name'] }}
                    </div>
                    <div style="border-bottom: 1px solid #000;">
                        {!! $p['packages']['description'] !!}
                    </div>
                @endforeach
            </div>
        </div>
        <div class="row">
            <p style="text-indent: 30px;">I hereby affix my signature to prove that the above contract of agreement is true and correct. to ensure the date of the function, 
                the amount of <b>75% of the total amount</b> must be paid and to be deducted on the total amount. Upon signing the said contract, 
                no alteration shall be made of the entries. Full payment <b>shall not</b> be settled after the event/function done.</p>
            <p style="text-indent: 30px;">In case of cancellation, contractiong person will be given at least (1) week prior to the event/function to notify BANANA DRIVE CATERING Services
                and 5% of the total amount shall be deducted automatically as cancellation fee.</p>
        </div>
        <br><br><br>
        <div class="row text-center">
            <div class="col-md-4">
                <b style="border-bottom: 1px solid #000; text-transform: uppercase;">{{ $name }}</b><br>
                Contacting Person
            </div>
            <div class="col-md-8">
                <b style="border-bottom: 1px solid #000;">ADELAIDA P. LUMAIN</b><br>
                BDCS Management
            </div>
        </div>
    </div>
</body>
</html>