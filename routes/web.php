<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('public.home');
});

Auth::routes();

Route::get('/test', ['uses' => 'HomeController@test']);

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/menu', ['uses' => 'HomeController@menu']);
Route::get('/contact', ['uses' => 'HomeController@contact']);
Route::get('/about-us', ['uses' => 'HomeController@about']);
Route::get('/dashboard', ['uses' => 'HomeController@dashboard']);
Route::get('/get-user', ['uses' => 'DashboardController@get_user']);
Route::get('/get-admin', ['uses' => 'DashboardController@get_admin']);
Route::get('/reservation/create', ['uses' => 'DashboardController@create']);
Route::get('/get-my-reservations', ['uses' => 'ReservationController@get_my_reservations']);
Route::get('/get-log/{id}', ['uses' => 'StatusLogController@index']);
Route::get('/check-reservation', ['uses' => 'ReservationController@check_reservation']);
Route::get('/date-checker', ['uses' => 'ReservationController@date_checker']);
Route::get('/show-receipt/{id}', ['uses' => 'InvoiceController@receipt']);
Route::get('/show-receipt-pdf/{id}', ['uses' => 'InvoiceController@generate_pdf']);
Route::get('/show-contract/{id}', ['uses' => 'ContractController@index']);
Route::get('/get-my-account', ['uses' => 'RefundAccountController@get_my_account']);
Route::get('/get-reservation/{id}/edit', ['uses' => 'ReservationController@get_reservation']);
Route::get('/get-reservation-date', ['uses' => 'ReservationController@get_reservation_date']);
Route::get('/get-calendar-date', ['uses' => 'CalendarController@get_calendar_date']);
Route::get('/paid-amount', ['uses' => 'PaymentController@payment_reservation']);
Route::get('/get-disabled-dates', ['uses' => 'DisabledDatesController@get_disabled_dates']);

Route::put('/update-user/{user}', ['uses' => 'DashboardController@update_user']);
Route::put('/update-user-details/{user_detail}', ['uses' => 'DashboardController@update_user_details']);
Route::put('/cancel-reservation/{reservation}', ['uses' => 'ReservationController@cancel_reservation']);
Route::put('/cancel-payment/{payment}', ['uses' => 'PaymentController@cancel_payment']);
Route::put('/update-password/{id}', ['uses' => 'UserController@reset']);

Route::post('/refund-payment/{payment}', ['uses' => 'PaymentController@refund_payment']);
Route::post('/send-payment/{id}', ['uses' => 'PaymentController@pupdate']);
Route::post('/save-user-details', ['uses' => 'DashboardController@save_details']);
Route::post('/add-reservation', ['uses' => 'ReservationController@add_reservation']);
Route::post('/reservation-fee', ['uses' => 'PaymentController@reservation_fee']);
Route::post('/add-request-payment', ['uses' => 'PaymentController@add_request_payment']);
Route::post('/admin-send-payment', ['uses' => 'PaymentController@admin_send_payment']);
// Route::post('/add-payment', ['uses' => 'PaymentController@add_payment']);

Route::resource('contact-us', 'ContactUsController');
Route::resource('notifications', 'NotificationController');
Route::resource('histories', 'HistoryController');
Route::resource('payments', 'PaymentController');
Route::resource('reservations', 'ReservationController');
Route::resource('disable-date', 'DisabledDatesController');
// Route::resource('contract', 'ContractController');

Route::get('/get-categories', ['uses' => 'MenuController@get_categories']);
Route::get('/get-foods', ['uses' => 'MenuController@get_foods']);
Route::get('/get-packages', ['uses' => 'PackageController@get_packages']);
Route::get('/get-packages-html', ['uses' => 'PackageController@get_packages_html']);
Route::get('/get-services', ['uses' => 'PackageController@get_services']);
Route::get('get-provinces', ['uses' => 'HomeController@get_provinces']);
Route::get('get-municipalities', ['uses' => 'HomeController@get_municipalities']);
Route::get('get-barangays', ['uses' => 'HomeController@get_barangays']);

Route::group(['prefix' => 'validator'], function() {
	Route::get('unique', ['uses' => 'ValidatorController@unique']);
});

// Admin
Route::group(['prefix' => 'admin'], function() {
    Route::get('/', ['as' => 'admin-login', 'uses' => 'Admin\LoginController@showLoginForm']);
    Route::get('home', ['as' => 'admin-home', 'uses' => 'Admin\HomeController@index']);
    Route::get('users', ['uses' => 'Admin\UserController@index']);
    Route::get('get-users', ['uses' => 'Admin\UserController@get_users']);
    Route::get('get-admins-users', ['uses' => 'Admin\UserController@get_admins_users']);
    Route::get('get-user-details', ['uses' => 'Admin\UserDetailsController@get_user_details']);
    Route::get('get-categories', ['uses' => 'Admin\CategoryController@get_categories']);
    Route::get('get-food-drinks', ['uses' => 'Admin\FoodDrinkController@get_food_drinks']);
    Route::get('get-materials', ['uses' => 'Admin\MaterialController@get_materials']);
    Route::get('get-services', ['uses' => 'Admin\ServiceController@get_services']);
    Route::get('get-packages', ['uses' => 'Admin\PackageController@get_packages']);
    Route::get('get-provinces', ['uses' => 'Admin\ProvinceController@get_provinces']);
    Route::get('get-provinces-municipalities', ['uses' => 'Admin\ProvinceController@get_provinces_municipalities']);
    Route::get('get-category-foods', ['uses' => 'Admin\PackageController@get_category_foods']);
    Route::get('get-reservations', ['uses' => 'Admin\ReservationController@get_reservations']);
    Route::get('get-reservation/{id}', ['uses' => 'Admin\ReservationController@get_reservation']);
    Route::get('get-reservations_report', ['uses' => 'Admin\ReportController@get_reservations_report']);
    Route::get('get-bar-report', ['uses' => 'Admin\ReportController@get_bar_report']);
    Route::get('get-refunds', ['uses' => 'RefundAccountController@get_refunds']);
    Route::get('calendar', ['uses' => 'CalendarController@index']);

    Route::get('sales-report', ['uses' => 'SalesReportController@index']);
    Route::get('fee-management', ['uses' => 'FeeManagementController@index']);
    Route::get('fee-management-data', ['uses' => 'FeeManagementController@fee_management']);
    Route::get('get-data', ['uses' => 'SalesReportController@get_report']);
    
    Route::resource('user-details', 'Admin\UserDetailsController');
    Route::resource('categories', 'Admin\CategoryController');
    Route::resource('food-drinks', 'Admin\FoodDrinkController');
    Route::resource('packages', 'Admin\PackageController');
    Route::resource('materials', 'Admin\MaterialController');
    Route::resource('services', 'Admin\ServiceController');
    Route::resource('provinces', 'Admin\ProvinceController');
    Route::resource('reservations', 'Admin\ReservationController');
    Route::resource('refunds', 'RefundAccountController');

    Route::post('food-drinks/photo/{id}', 'Admin\FoodDrinkController@update_photo');
    Route::post('upload-excel', 'Admin\ProvinceController@update_excel');

    Route::post('save-user', ['uses' => 'Admin\UserController@store']);
    Route::put('update-user', ['uses' => 'Admin\UserController@update']);
    Route::delete('delete-user', ['uses' => 'Admin\UserController@destroy']);

    Route::post('login', ['as' => 'login-admin', 'uses' => 'Admin\LoginController@login']);
    Route::post('logout', ['as' => 'logout-admin', 'uses' => 'Admin\LoginController@logout']);

    Route::put('/update-status/{id}', ['uses' => 'Admin\ReservationController@update_status']);
    Route::put('/update-admin/{id}', ['uses' => 'Admin\UserController@update_admin']);
});